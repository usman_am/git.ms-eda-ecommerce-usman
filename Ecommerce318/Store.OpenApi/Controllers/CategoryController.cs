﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _service;
        private readonly ILogger<CategoryController> _logger;
        private IValidator<CategoryDto> _validator;

        public CategoryController(ICategoryService service, ILogger<CategoryController>
            logger, IValidator<CategoryDto> validator)
        {
            _service = service;
            _logger = logger;
            _validator = validator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CategoryDto payload,
            CancellationToken cancellationToken)
        {
            try
            {
                ValidationResult result = await _validator.ValidateAsync(payload);
                if(!result.IsValid)
                    return BadRequest(result);
           
                var dto = await _service.AddCategory(payload);
                if (dto != null)
                    return Ok(dto);
            }
            catch (OperationCanceledException ex) when
            (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Put(Guid id, [FromBody] CategoryDto payload,
            CancellationToken cancellationToken)
        {
            try
            {
                if (payload != null)
                {
                    payload.Id = id;
                    var isUpdated = await _service.UpdateCategory(payload);
                    if (isUpdated)
                        return Ok(isUpdated);
                }
            }
            catch (OperationCanceledException ex) when
                (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id,CancellationToken cancellationToken)
        {
            try
            {
                if (id != null)
                {
                    var isDeleted = await _service.DeleteCategory(id);
                    if (isDeleted)
                        return Ok(isDeleted);
                }
            }
            catch (OperationCanceledException ex) when
                (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }
    }
}
