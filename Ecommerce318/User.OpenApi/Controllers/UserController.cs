﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using User.Domain.Dtos;
using User.Domain.Services;

namespace User.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;
        private readonly ILogger<UserController> _logger;

        public UserController(IUserService service,
            ILogger<UserController> logger)
        {
            _service = service;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpGet("GetById")]
        public async Task<IActionResult> GetById(Guid id)
        {
            if (id != null)
            {
                var isGeted = await _service.GetUserById(id);
                if (isGeted != null)
                    return Ok(isGeted);
            }
            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserDto payload,
            CancellationToken cancellationToken)
        {
            try
            {
                var dto = await _service.AddUser(payload);
                if (dto != null)
                    return Ok(dto);
            }
            catch (OperationCanceledException ex) when
            (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Put(Guid id, [FromBody] UserDto payload,
           CancellationToken cancellationToken)
        {
            try
            {
                if (payload != null)
                {
                    payload.Id = id;
                    var isUpdated = await _service.UpdateUser(payload);
                    if (isUpdated)
                        return Ok(isUpdated);
                }
            }
            catch (OperationCanceledException ex) when
                (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id, CancellationToken cancellationToken)
        {
            try
            {
                if (id != null)
                {
                    var isDeleted = await _service.DeleteUser(id);
                    if (isDeleted)
                        return Ok(isDeleted);
                }
            }
            catch (OperationCanceledException ex) when
                (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }
    }
}
