using Cart.Domain;
using Microsoft.EntityFrameworkCore;
using Cart.Domain.Repositories;
using Cart.Domain.MapProfile;
using Framework.Core.Events;
using Framework.Kafka;
using Cart.Domain.Services;
using Cart.GraphQL.Scema.Queries;
using Cart.GraphQL.Scema.Mutations;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDomainContext(option =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    option.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection
        ("Cart_Db_Conn").Value);

    option.EnableSensitiveDataLogging(false)
    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

builder.Services
    .AddScoped<ICartRepository, CartRepository>()
    .AddScoped<ICartService, CartService>();
builder.Services
  .AddScoped<ICartProductRepository, CartProductRepository>()
  .AddScoped<ICartProductService, CartProductService>();
builder.Services
    .AddScoped<IProductRepository, ProductRepository>()
    .AddScoped<IProductService, ProductService>();

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddCart();

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoCart>();
    config.AddProfile<EntityToDtoCartProduct>();
    config.AddProfile<EntityToDtoProduct>();

});

builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services
    .AddScoped<Query>()
    .AddScoped<CartQuery>()
    .AddScoped<CartProductQuery>()
    .AddScoped<Mutation>()
    .AddScoped<CartMutation>()
    .AddScoped<CartProductMutation>()
    .AddScoped<ICartRepository, CartRepository>()
    .AddScoped<ICartService, CartService>()
    .AddScoped<ICartProductRepository, CartProductRepository>()
    .AddScoped<ICartProductService, CartProductService>()
    .AddGraphQLServer()
    .AddQueryType<Query>()
    .AddTypeExtension<CartQuery>()
    .AddTypeExtension<CartProductQuery>()
    .AddMutationType<Mutation>()
    .AddTypeExtension<CartMutation>()
    .AddTypeExtension<CartProductMutation>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGraphQL();

app.Run();
