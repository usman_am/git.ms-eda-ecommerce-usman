#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Cart.GraphQL/Cart.GraphQL.csproj", "Cart.GraphQL/"]
COPY ["Cart.Domain/Cart.Domain.csproj", "Cart.Domain/"]
COPY ["Framework.Auth/Framework.Auth.csproj", "Framework.Auth/"]
COPY ["Framework.Core/Framework.Core.csproj", "Framework.Core/"]
COPY ["Framework.Kafka/Framework.Kafka.csproj", "Framework.Kafka/"]
COPY ["User.Domain/User.Domain.csproj", "User.Domain/"]
RUN dotnet restore "Cart.GraphQL/Cart.GraphQL.csproj"
COPY . .
WORKDIR "/src/Cart.GraphQL"
RUN dotnet build "Cart.GraphQL.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Cart.GraphQL.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Cart.GraphQL.dll"]