﻿using Cart.Domain.Dtos;
using Cart.Domain.Services;

namespace Cart.GraphQL.Scema.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class CartProductMutation
    {
        private readonly ICartProductService _service;

        public CartProductMutation(ICartProductService service)
        {
            _service = service;
        }

        public async Task<CartProductDto> AddCartProductAsync(CartProductTypeInput attribute)
        {
            CartProductDto dto = new CartProductDto();
            dto.CartId = attribute.CartId;
            dto.ProductId = attribute.ProductId;
            dto.Quatity = attribute.Quantity;
            var result = await _service.AddCartProduct(dto);
            return result;
        }

        public async Task<CartProductDto> EditCartProductAsync(Guid id, CartProductTypeInput attribute)
        {
            CartProductDto dto = new CartProductDto();
            dto.Id = id;
            dto.CartId = attribute.CartId;
            dto.ProductId = attribute.ProductId;
            dto.Quatity = attribute.Quantity;
            var result = await _service.UpdateCartProduct(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));
            }
            return dto;
        }
    }
}
