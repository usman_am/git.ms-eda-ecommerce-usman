﻿using Cart.Domain;
using Cart.Domain.Dtos;
using Cart.Domain.Services;

namespace Cart.GraphQL.Scema.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class CartMutation
    {
        private readonly ICartService _service;

        public CartMutation(ICartService service)
        {
            _service = service;
        }

        public async Task<CartDto> AddCartAsync(CartTypeInput attribute)
        {
            CartDto dto = new CartDto();
            dto.CustomerId = attribute.CustomerId;
            var result = await _service.AddCart(dto);
            return result;
        }

        public async Task<CartDto> ConfirmCartAsync(Guid id)
        {
            CartDto dto = new CartDto();
            dto.Id = id;
            var result = await _service.UpdateCartStatus(id, CartStatusEnum.Confirmed);
            if (result) { dto = await _service.GetCartById(id); }
            return dto;
        }

        public async Task<CartDto> EditCartAsync(Guid id, CartTypeInput attribute)
        {
            CartDto dto = new CartDto();
            dto.Id = id;
            dto.CustomerId = attribute.CustomerId;
            var result = await _service.UpdateCart(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));
            }
            return dto;
        }
        public async Task<CartDto> UpdateCartStatusAsync(Guid id, CartStatusEnum status)
        {
            var result = await _service.UpdateCartStatus(id, status);

            if (result)
                return await _service.GetCartById(id);

            throw new GraphQLException(new Error("Attribute not found", "404"));
        }

        public async Task<bool> DeleteCartAsync(Guid id)
        {
            var result = await _service.DeleteCart(id);

            if (result)
                return result;

            throw new GraphQLException(new Error("Attribute not found", "404"));
        }
    }
}
