﻿using Cart.Domain.Dtos;
using Cart.Domain.Services;

namespace Cart.GraphQL.Scema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class CartProductQuery
    {
        private readonly ICartProductService _service;

        public CartProductQuery(ICartProductService service)
        {
            _service = service;
        }
        public async Task<IEnumerable<CartProductDto>> GetAllCartProductAsync()
        {
            IEnumerable<CartProductDto> result = await _service.All();
            return result;
        }

        public async Task<CartProductDto> GetCartProductByIdAsync(Guid id)
        {
            return await _service.GetCartProductById(id);
        }
    }
}
