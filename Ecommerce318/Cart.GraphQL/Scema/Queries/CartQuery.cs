﻿using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using Cart.Domain.Services;

namespace Cart.GraphQL.Scema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class CartQuery
    {
        private readonly ICartService _service;

        public CartQuery(ICartService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<CartDto>> GetAllCartAsync()
        {
            IEnumerable<CartDto> result = await _service.All();
            return result;
        }

        public async Task<IEnumerable<CartEntity>> GetAllInAsync()
        {
            return await _service.AllIn();
        }

        public async Task<CartDto> GetCartByIdAsync(Guid id)
        {
            return await _service.GetCartById(id);
        }
    }
}
