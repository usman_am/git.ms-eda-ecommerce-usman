﻿using Cart.Domain.Projections;
using Framework.Core.Projection;
using Microsoft.Extensions.DependencyInjection;

namespace Cart.Domain
{
    public static class CartServices
    {
        public static IServiceCollection AddCart(this IServiceCollection services) =>
            services.AddProjections();

        private static IServiceCollection AddProjections(this IServiceCollection services) =>
            services
            .Projection(builder => builder
            .AddOn<ProductCreated>(ProductProjection.Handle)
            .AddOn<ProductUpdated>(ProductProjection.Handle)
            .AddOn<ProductStatusChanged>(ProductProjection.Handle)
            .AddOn<ProductDeleted>(ProductProjection.Handle)
            .AddOn<UserCreated>(UserProjection.Handle)
            .AddOn<UserUpdated>(UserProjection.Handle)
            .AddOn<UserStatusChanged>(UserProjection.Handle)
            .AddOn<UserDeleted>(UserProjection.Handle));
    }
}
