﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;

namespace Cart.Domain.MapProfile
{
    public class EntityToDtoCart: Profile
    {
        public EntityToDtoCart(): base("Entity to dto Cart")
        {
            CreateMap<CartEntity, CartDto>();
            CreateMap<CartDto, CartEntity>();
        }
    }
}
