﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.MapProfile
{
    public class EntityToDtoCartProduct: Profile
    {
        public EntityToDtoCartProduct(): base("Entity to dto CartProduct")
        {
            CreateMap<CartProductEntity, CartProductDto>();
            CreateMap<CartProductDto, CartProductEntity>();
        }
    }
}
