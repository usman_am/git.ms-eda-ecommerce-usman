﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.MapProfile
{
    public class EntityToDtoProduct : Profile
    {
        public EntityToDtoProduct() : base("Entity to dto Product")
        {
            CreateMap<ProductEntity, ProductDto>();
            CreateMap<ProductDto, ProductEntity>();
        }
    }
}
