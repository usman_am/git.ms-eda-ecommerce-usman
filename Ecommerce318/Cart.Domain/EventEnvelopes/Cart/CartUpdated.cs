﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.EventEnvelopes.Cart
{
    public record CartUpdated(
        Guid Id,
        Guid CustomerId,
        DateTime Modified
        )
    {
       public static CartUpdated Update(
            Guid id,
            Guid customerId,
            DateTime modified
            ) => new(id, customerId, modified);
    }
}
