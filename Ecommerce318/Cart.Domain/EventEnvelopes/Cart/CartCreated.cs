﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.EventEnvelopes.Cart
{
    public record CartCreated(
        Guid Id,
        Guid CustomerId,
        CartStatusEnum Status,
        DateTime Modified)
    {
        public static CartCreated Create(
            Guid id,
            Guid customerId,
            CartStatusEnum status,
            DateTime modified
            ) => new(id, customerId, status, modified);
    }

    
}
