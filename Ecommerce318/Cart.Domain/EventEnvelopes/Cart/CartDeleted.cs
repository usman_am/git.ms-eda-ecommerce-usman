﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.EventEnvelopes.Cart
{
    public record CartDeleted(
        Guid Id,
        CartStatusEnum Status,
        DateTime Modified)
    {
        public static CartDeleted Delete(
             Guid id,
            CartStatusEnum status,
            DateTime modified
            ) => new(id, status, modified);
    }
}
