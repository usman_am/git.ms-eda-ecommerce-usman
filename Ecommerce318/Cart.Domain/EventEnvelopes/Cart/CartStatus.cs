﻿

namespace Cart.Domain.EventEnvelopes.Cart
{
    public record CartStatus(
        Guid Id,
        Guid CustomerId,
        List<CartProductItem> CartProducts,
        CartStatusEnum Status,
        DateTime Modified
        )
    {
        public static CartStatus EditStatus(
             Guid id,
             Guid customerId,
             List<CartProductItem> cartProducts,
             CartStatusEnum status,
             DateTime modified
             ) => new(id,customerId, cartProducts, status, modified);
    }

    public class CartProductItem
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }
}
