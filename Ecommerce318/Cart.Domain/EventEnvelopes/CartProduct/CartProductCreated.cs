﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.EventEnvelopes.CartProduct
{
    public record CartProductCreated(
        Guid Id,
        Guid CartId,
        Guid ProductId,
        string SKU,
        string Name,
        int Quantity,
        decimal Price,
        DateTime Modified)
    {
        public static CartProductCreated Create(
            Guid id,
            Guid cartId,
            Guid productId,
            string sKU,
            string name,
            int quantity,
            decimal price,
            DateTime modified
            ) => new(id, cartId, productId, sKU, name,quantity,price, modified);
    }
}
