﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using Cart.Domain.Repositories;
using Framework.Core.Events.Externals;
using Framework.Core.Events;
using Cart.Domain.EventEnvelopes.CartProduct;

namespace Cart.Domain.Services
{
    public interface ICartProductService
    {
        Task<IEnumerable<CartProductDto>> All();
        Task<CartProductDto> AddCartProduct(CartProductDto dto);
        Task<bool> UpdateCartProduct(CartProductDto dto);
        Task<CartProductDto> GetCartProductById(Guid id);
    }
    public class CartProductService: ICartProductService
    {
        private IProductRepository _productRepository;
        private ICartProductRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;
        public CartProductService(ICartProductRepository repository, IMapper mapper, 
            IExternalEventProducer externalEventProducer, IProductRepository productRepository)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
            _productRepository = productRepository;
        }

        public async Task<CartProductDto> AddCartProduct(CartProductDto dto)
        {
            if (dto != null)
            {
                var product = await _productRepository.GetById(dto.ProductId);
                if (product == null) { return null; }

                dto.Price = product.Price;
                dto.Name = product.Name;
                dto.SKU = product.SKU;

                var existCp = await _repository.GetByCartId(dto.CartId, dto.ProductId);
                if(existCp != null)
                {
                    if(product.Stock >= dto.Quatity + existCp.Quatity)
                    {
                        existCp.Quatity = dto.Quatity + existCp.Quatity;
                    }
                    else
                    {
                        existCp.Quatity = product.Stock;
                    }
                    var entity = await _repository.Update(existCp);
                    var result = await _repository.SaveChangesAsyc();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<CartProductUpdated>(
                        CartProductUpdated.Update(
                            entity.Id,
                            entity.CartId,
                            entity.ProductId,
                            entity.SKU,
                            entity.Name,
                            entity.Quatity,
                            entity.Price,
                            entity.Modified
                            )
                        );
                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                        return _mapper.Map<CartProductDto>(entity);
                    }
                }
                else
                {
                    if(product.Stock < dto.Quatity || product.Stock<1 || dto.Quatity<1) { return null; }
                    var dtoToEntity = _mapper.Map<CartProductEntity>(dto);
                    var entity = await _repository.Add(dtoToEntity);
                    var result = await _repository.SaveChangesAsyc();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<CartProductCreated>(
                            CartProductCreated.Create(
                            entity.Id,
                            entity.CartId,
                            entity.ProductId,
                            entity.SKU,
                            entity.Name,
                            entity.Quatity,
                            entity.Price,
                            entity.Modified
                            )
                        );
                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                        return _mapper.Map<CartProductDto>(entity);
                    }
                } 
            }
            return new CartProductDto();
        }

        public async Task<IEnumerable<CartProductDto>> All()
        {
            return _mapper.Map<IEnumerable<CartProductDto>>(await _repository.GetAll());
        }

        public async Task<bool> UpdateCartProduct(CartProductDto dto)
        {
            if (dto != null)
            {
                var attribute = await _repository.GetById(dto.Id);
                if (attribute != null)
                {
                    var entity = await _repository.Update(_mapper.Map<CartProductEntity>(dto));
                    var result = await _repository.SaveChangesAsyc();

                    if (result > 0) 
                    {
                        var externalEvent = new EventEnvelope<CartProductUpdated>(
                        CartProductUpdated.Update(
                            entity.Id,
                            entity.CartId,
                            entity.ProductId,
                            entity.SKU,
                            entity.Name,
                            entity.Quatity,
                            entity.Price,
                            entity.Modified
                            )
                        );
                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                        return true;
                    } 
                }
            }
            return false;
        }

        public async Task<CartProductDto> GetCartProductById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CartProductDto>(result);
                }
            }
            return null;
        }
    }

   
}
