﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using Cart.Domain.EventEnvelopes.Cart;
using Cart.Domain.Repositories;
using Framework.Core.Events;
using Framework.Core.Events.Externals;

namespace Cart.Domain.Services
{
    public interface ICartService
    {
        Task<IEnumerable<CartDto>> All();
        Task<IEnumerable<CartEntity>> AllIn();
        Task<CartDto> GetCartById(Guid id);
        Task<CartDto> AddCart(CartDto dto);
        Task<bool> UpdateCart(CartDto dto);
        Task<bool> UpdateCartStatus(Guid id, CartStatusEnum status);
        Task<bool> DeleteCart(Guid id);
    }
    public class CartService: ICartService
    {
        private ICartRepository _repository;
        private ICartProductRepository _cartproductrepository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public CartService(ICartRepository repository, ICartProductRepository cartProductRepository,
            IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _cartproductrepository = cartProductRepository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<CartDto> AddCart(CartDto dto)
        {
            if (dto != null)
            {
                dto.Status = CartStatusEnum.Pending;
                var dtoToEntity = _mapper.Map<CartEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsyc();

                if (result > 0)
                {
                var externalEvent = new EventEnvelope<CartCreated>(
                CartCreated.Create(
                    entity.Id,
                    entity.CustomerId,
                    entity.Status,
                    entity.Modified
                )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                return _mapper.Map<CartDto>(entity);
                }
            }
            return new CartDto();
        }

        public async Task<IEnumerable<CartDto>> All()
        {
            return _mapper.Map<IEnumerable<CartDto>>(await _repository.GetAll());
        }

        public async Task<bool> UpdateCart(CartDto dto)
        {
            if (dto != null)
            {
                var attribute = await _repository.GetById(dto.Id);
                if (attribute != null)
                {
                    var entity = await _repository.Update(_mapper.Map<CartEntity>(dto));
                    var result = await _repository.SaveChangesAsyc();

                    if (result > 0)
                    {
                    var externalEvent = new EventEnvelope<CartUpdated>(
                    CartUpdated.Update(
                    entity.Id,
                    entity.CustomerId,
                    entity.Modified
                    )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateCartStatus(Guid id, CartStatusEnum status)
        {
            bool res = true;
            var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.Status = status;
                var entity = await _repository.Update(attribute);
                var result = await _repository.SaveChangesAsyc();

                if (result > 0)
                {
                    List<CartProductItem> list = new List<CartProductItem>();
                    if(status == CartStatusEnum.Confirmed)
                    {
                        var cartProducts = await _cartproductrepository.GetByCartId(entity.Id);
                        foreach ( var product in cartProducts )
                        {
                            list.Add(new CartProductItem()
                            {
                                Id = product.Id,
                                ProductId = product.ProductId,
                                Price = product.Price,
                                Quantity = product.Quatity
                            });
                        }
                    }

                var externalEvent = new EventEnvelope<CartStatus>(
                CartStatus.EditStatus(
                   entity.Id,
                   entity.CustomerId,
                   list,
                   entity.Status,
                   entity.Modified
                )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                return true;
                }
            }
            return false;
        }

        public async Task<bool> DeleteCart(Guid id)
        {
            var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.Status = CartStatusEnum.Canceled;
                var entity = await _repository.Update(attribute);
                var result = await _repository.SaveChangesAsyc();

                if (result > 0) {
                var externalEvent = new EventEnvelope<CartDeleted>(
                CartDeleted.Delete(
                   entity.Id,
                   entity.Status,
                   entity.Modified
                )
                );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                return true;
                } 
            }
            return false;
        }

        public async Task<CartDto> GetCartById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CartDto>(result);
                }
            }
            return null;
        }

        public async Task<IEnumerable<CartEntity>> AllIn()
        {
            return await _repository.GetAll();
        }
    }

   
}
