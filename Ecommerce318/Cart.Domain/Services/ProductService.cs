﻿using AutoMapper;
using Framework.Core.Events.Externals;
using Framework.Core.Events;
using Cart.Domain.Dtos;
using Cart.Domain.Repositories;
using Cart.Domain.Entities;

namespace Cart.Domain.Services
{
    public interface IProductService
    {
        Task<ProductDto> GetProductById(Guid id);
    }
    public class ProductService : IProductService
    {
        private IProductRepository _repository;
        private readonly IMapper _mapper;
        public ProductService(IProductRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<ProductDto> GetProductById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<ProductDto>(result);
                }
            }
            return null;
        }
    }
}