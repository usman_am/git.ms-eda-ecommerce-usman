﻿using Cart.Domain.Entities.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.Entities
{
    public class CartDbContext : DbContext
    {
        public CartDbContext(DbContextOptions<CartDbContext> options): base(options) { }
        
        public DbSet<CartEntity> Carts { get; set; }
        public DbSet<CartProductEntity> CartProducts { get; set; }
        public DbSet<ProductEntity> Products { get; set; }
        public DbSet<UserEntity> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new CartCofiguration());
            modelBuilder.ApplyConfiguration(new CartProductConfiguration());
            modelBuilder.ApplyConfiguration(new  ProductConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }

        public static DbContextOptions<CartDbContext> OnConfigure()
        {
            var optionsBuilder = new DbContextOptionsBuilder<CartDbContext>();
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            optionsBuilder.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Cart_Db_Conn").Value);

            return optionsBuilder.Options;
        }
    }
}
