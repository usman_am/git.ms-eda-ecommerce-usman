﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.Entities.Configurations
{
    public class CartCofiguration : IEntityTypeConfiguration<CartEntity>
    {
        public void Configure(EntityTypeBuilder<CartEntity> builder)
        {
            builder.ToTable("Carts");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id);
            builder.Property(x => x.CustomerId);
            builder.Property(x => x.Status);
        }
    }

    public class CartProductConfiguration : IEntityTypeConfiguration<CartProductEntity>
    {
        public void Configure(EntityTypeBuilder<CartProductEntity> builder)
        {
            builder.ToTable("CartProducts");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).IsRequired();
            builder.Property(x => x.CartId).IsRequired();
            builder.Property(x => x.ProductId).IsRequired();
            builder.Property(x => x.SKU).IsRequired();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.Quatity).IsRequired();
            builder.Property(x => x.Price).HasPrecision(18, 2).IsRequired();
        }
    }

    public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).IsRequired();
            builder.Property(x => x.UserName).IsRequired();
            builder.Property(x => x.Password).IsRequired();
            builder.Property(x => x.FirstName).IsRequired();
            builder.Property(x=> x.LastName);
            builder.Property(x => x.Email);
            builder.Property(x => x.Type).IsRequired();
            builder.Property(x => x.Status).IsRequired();
        }
    }

    public class ProductConfiguration : IEntityTypeConfiguration<ProductEntity>
    {
        public void Configure(EntityTypeBuilder<ProductEntity> builder)
        {
            builder.ToTable("Products");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.CategoryId).IsRequired();
            builder.Property(e => e.AttributeId).IsRequired();

            builder.Property(e => e.SKU).HasMaxLength(20).IsRequired();
            builder.Property(e => e.Name).HasMaxLength(30).IsRequired();
            builder.Property(e => e.Description).HasMaxLength(255).IsRequired();
            builder.Property(e => e.Price).HasPrecision(18, 2);
            builder.Property(e => e.Volume).HasPrecision(18, 2);
            builder.Property(e => e.Sold).IsRequired();
            builder.Property(e => e.Stock).IsRequired();
            builder.Property(e => e.Status).IsRequired();
        }
    }
}
