﻿using Cart.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Cart.Domain.Repositories
{
    public interface ICartRepository
    {
        Task<CartEntity> Add(CartEntity entity);
        Task<IEnumerable<CartEntity>> GetAll();
        Task<CartEntity> GetById(Guid id);
        Task<CartEntity> Update(CartEntity entity);
        Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default);
    }
    public class CartRepository: ICartRepository
    {
        protected readonly CartDbContext _context;

        public CartRepository(CartDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<CartEntity> Add(CartEntity entity)
        {
            _context.Set<CartEntity>().Add(entity);
            return entity;
        }

        public async Task<IEnumerable<CartEntity>> GetAll()
        {
            return await _context.Set<CartEntity>().Include(o => o.CartProducts).ToListAsync();
        }

        public async Task<CartEntity> GetById(Guid id)
        {
            return await _context.Set<CartEntity>().FindAsync(id);
        }

        public async Task<CartEntity> Update(CartEntity entity)
        {
            _context.Set<CartEntity>().Update(entity);
            return entity;
        }

        public async Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    
}
