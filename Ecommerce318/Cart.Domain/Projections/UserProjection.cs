﻿using Cart.Domain.Entities;
using Framework.Core.Events;

namespace Cart.Domain.Projections
{
    public record UserCreated(
        Guid? Id,
        string UserName,
        string Password,
        string FirstName,
        string LastName,
        string Email,
        UserTypeEnum Type,
        ProductStatusEnum Status,
        DateTime Modified
    );

    public record UserUpdated(
        Guid? Id,
        string UserName,
        string Password,
        string FirstName,
        string LastName,
        string Email,
        UserTypeEnum Type,
        DateTime Modified
    );

    public record UserStatusChanged(
        Guid? Id,
        ProductStatusEnum Status,
        DateTime Modified
    );

    public record UserDeleted(
        Guid? Id,
        ProductStatusEnum Status,
        DateTime Modified
    );

    public class UserProjection
    {
        private readonly CartDbContext _context;
        public UserProjection()
        {

        }

        public static bool Handle(EventEnvelope<UserCreated> eventEnvelope)
        {
            var (id, username, password, firstName, lastName,email, type, status, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UserEntity entity = new UserEntity()
                {
                    Id = (Guid)id,
                    UserName = username,
                    Password = password,
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email,
                    Type = type,
                    Status = status,
                    Modified = modified
                };

                context.Users.Add(entity);
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<UserUpdated> eventEnvelope)
        {
            var (id, username, password, firstName, lastName, email, type, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);
                entity.UserName = username;
                entity.Password = password;
                entity.FirstName = firstName;
                entity.LastName = lastName;
                entity.Email = email;
                entity.Type = type;
                entity.Modified = modified;
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<UserStatusChanged> eventEnvelope)
        {
            var (id, status, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);
                entity.Status = status;
                entity.Modified = modified;
                context.SaveChanges();
            }
            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<UserDeleted> eventEnvelope)
        {
            var (id, status, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);
                entity.Status = status;
                entity.Modified = modified;
                context.SaveChanges();
            }
            return true; //new AttributeCreated(id, type, unit, status, modified);
        }
    }
}
