﻿using Payment.Domain;
using Payment.Domain.Dtos;
using Payment.Domain.Services;

namespace Payment.GraphQL.Scema.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class PaymentMutation
    {
        private readonly IPaymentService _service;

        public PaymentMutation(IPaymentService service)
        {
            _service = service;
        }

       /* public async Task<PaymentDto> AddPaymentAsync(PaymentTypeInput attribute)
        {
            PaymentDto dto = new PaymentDto();
            dto.CartId = attribute.CartId;
            dto.Total = attribute.Total;
            dto.Pay = attribute.Pay;
            dto.Status = attribute.Status;
            var result = await _service.AddPayment(dto);
            return result;
        }*/

        public async Task<PaymentDto> EditPaymentAsync(Guid id, PaymentTypeInput attribute)
        {
            PaymentDto dto = new PaymentDto();
            dto.Id = id;
            dto.CartId = attribute.CartId;
            dto.Total = attribute.Total;
            dto.Pay = attribute.Pay;
            dto.Status = attribute.Status;
            var result = await _service.UpdatePayment(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));
            }
            return dto;
        }
        public async Task<PaymentDto> UpdatePaymentStatusAsync(Guid id, PaymentStatusEnum status)
        {
            var result = await _service.UpdatePaymentStatus(id, status);

            if (result)
                return await _service.GetPaymentById(id);

            throw new GraphQLException(new Error("Attribute not found", "404"));
        }

        public async Task<bool> DeletePaymentAsync(Guid id)
        {
            var result = await _service.DeletePayment(id);

            if (result)
                return result;

            throw new GraphQLException(new Error("Attribute not found", "404"));
        }
    }
}
