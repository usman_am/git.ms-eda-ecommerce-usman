﻿using Payment.Domain;

namespace Payment.GraphQL.Scema.Mutations
{
    public class PaymentTypeInput
    {
        public Guid CartId { get; set; }
        public decimal Total { get; set; }
        public decimal Pay { get; set; }
        public CartStatusEnum Status { get; set; }
    }
}
