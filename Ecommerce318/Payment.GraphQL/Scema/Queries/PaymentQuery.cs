﻿using Payment.Domain.Dtos;
using Payment.Domain.Services;

namespace Payment.GraphQL.Scema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class PaymentQuery
    {
        private readonly IPaymentService _service;

        public PaymentQuery(IPaymentService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<PaymentDto>> GetAllPaymentAsync()
        {
            IEnumerable<PaymentDto> result = await _service.All();
            return result;
        }

        public async Task<PaymentDto> GetPaymentByIdAsync(Guid id)
        {
            return await _service.GetPaymentById(id);
        }
    }
}
