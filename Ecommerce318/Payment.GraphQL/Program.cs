using Framework.Core.Events;
using Framework.Kafka;
using Microsoft.EntityFrameworkCore;
using Payment.Domain;
using Payment.Domain.MapProfile;
using Payment.Domain.Repositories;
using Payment.Domain.Services;
using Payment.GraphQL.Scema.Mutations;
using Payment.GraphQL.Scema.Queries;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDomainContext(option =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    option.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection
        ("Payment_Db_Conn").Value);

    option.EnableSensitiveDataLogging(false)
    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

builder.Services
    .AddScoped<IPaymentRepository, PaymentRepository>()
    .AddScoped<IPaymentService, PaymentService>();
builder.Services
    .AddScoped<ICartRepository, CartRepository>()
    .AddScoped<ICartService, CartService>();

// Add services to the container.

builder.Services.AddControllers();
builder.Services.addPayment();

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoPayment>();
    config.AddProfile<EntityToDtoCart>();
});

builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services
    .AddScoped<Query>()
    .AddScoped<PaymentQuery>()
    .AddScoped<Mutation>()
    .AddScoped<PaymentMutation>()
    .AddScoped<IPaymentRepository, PaymentRepository>()
    .AddScoped<IPaymentService, PaymentService>()
    .AddGraphQLServer()
    .AddQueryType<Query>()
    .AddTypeExtension<PaymentQuery>()
    .AddMutationType<Mutation>()
    .AddTypeExtension<PaymentMutation>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGraphQL();

app.Run();
