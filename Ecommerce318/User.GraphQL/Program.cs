using Framework.Kafka;
using Microsoft.EntityFrameworkCore;
using User.Domain;
using User.Domain.MapProfile;
using User.Domain.Repositories;
using User.Domain.Services;
using User.GraphQL.Scema.Queries;
using User.GraphQL.Scema.Mutations;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDomainContext(option =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    option.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection
        ("User_Db_Conn").Value);

    option.EnableSensitiveDataLogging(false)
    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

builder.Services
    .AddScoped<IUserRepository, UserRepository>()
    .AddScoped<IUserService, UserService>();

// Add services to the container.
builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoUser>();
});

builder.Services.AddControllers();
builder.Services.AddKafkaProducer();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//add 12-06-2023 -authetication
builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
    .AddJwtBearer("Bearer", opt =>
    {
        var Configuration = builder.Configuration;
        opt.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = Configuration["JWT:ValidIssuer"],
            ValidAudience = Configuration["JWT:ValidAudience"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]))
        };

        opt.Events = new JwtBearerEvents
        {
            OnChallenge = context =>
            {
                context.Response.OnStarting(async () =>
                {
                    await context.Response.WriteAsync("Account not autherized");
                });
                return Task.CompletedTask;
            },
            OnForbidden = context =>
            {
                context.Response.OnStarting(async () =>
                {
                    await context.Response.WriteAsync("Account forbidden");
                });
                return Task.CompletedTask;
            }
        };
    });

//-------------

builder.Services
    .AddScoped<Query>()
    .AddScoped<AuthQuery>()
    .AddScoped<UserQuery>()
    .AddScoped<Mutation>()
    .AddScoped<UserMutation>()
    .AddScoped<IUserRepository, UserRepository>()
    .AddScoped<IUserService, UserService>()
    .AddGraphQLServer()
    .AddQueryType<Query>()
    .AddType<AuthQuery>()
    .AddAuthorization()
    .AddTypeExtension<UserQuery>()
    .AddMutationType<Mutation>()
    .AddTypeExtension<UserMutation>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

//add 12/06/2023
app.UseRouting();

app.UseAuthentication();
//-----------


app.UseAuthorization();

app.MapControllers();

app.MapGraphQL();

app.Run();
