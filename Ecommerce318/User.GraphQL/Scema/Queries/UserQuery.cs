﻿using HotChocolate.Authorization;
using System.Data;
using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Scema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class UserQuery
    {
        private readonly IUserService _service;

        public UserQuery(IUserService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<IEnumerable<UserDto>> GetAllUserAsync()
        {
            IEnumerable<UserDto> result = await _service.All();
            return result;
        }

        [Authorize(Roles = new[] { "customer" })]
        public async Task<UserDto> GetUserByIdAsync(Guid id)
        {
            return await _service.GetUserById(id);
        }
    }
}
