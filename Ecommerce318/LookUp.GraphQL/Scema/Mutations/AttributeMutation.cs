﻿using LookUp.Domain;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Scema.Mutations
{
    [ExtendObjectType(Name ="Mutation")]
    public class AttributeMutation
    {
        private readonly IAttributeService _service;

        public AttributeMutation(IAttributeService service)
        {
            _service = service;
        }

        public async Task<AttributeDto> AddAttributeAsync(AttributeTypeInput attribute)
        {
            AttributeDto dto = new AttributeDto();
            dto.Unit = attribute.Unit;
            dto.Type = attribute.Type;
            var result = await _service.AddAttribute(dto);
            return result;
        }

        public async Task<AttributeDto> EditAttributeAsync(Guid id, AttributeTypeInput attribute)
        {
            AttributeDto dto = new AttributeDto();
            dto.Id = id;
            dto.Unit = attribute.Unit;
            dto.Type = attribute.Type;
            var result = await _service.UpdateAttribute(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));
            }
            return dto;
        }
        public async Task<AttributeDto> UpdateAttributeStatusAsync(Guid id, LookUpStatusEnum status)
        {
            var result = await _service.UpdateAttributeStatus(id, status);

            if (result)
                return await _service.GetAttributeById(id);

            throw new GraphQLException(new Error("Attribute not found", "404"));
        }

        public async Task<bool> DeleteAttributeAsync(Guid id)
        {
            var result = await _service.DeleteAttribute(id);

            if (result)
                return result;

            throw new GraphQLException(new Error("Attribute not found", "404"));
        }
    }
}
