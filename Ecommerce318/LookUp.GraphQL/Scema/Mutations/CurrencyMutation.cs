﻿using LookUp.Domain;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Scema.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class CurrencyMutation
    {
        private readonly ICurrencyServices _service;

        public CurrencyMutation(ICurrencyServices services)
        {
            _service = services;
        }

        public async Task<CurrencyDto> AddCurrencyAsync(CurrencyTypeInput currency)
        {
            CurrencyDto dto = new CurrencyDto();
            dto.Name = currency.Name;
            dto.Code = currency.Code;
            dto.Symbol = currency.Symbol;
            var result = await _service.AddCurrency(dto);
            return result;
        }

        public async Task<CurrencyDto> EditCurrencyAsync(Guid id, CurrencyTypeInput currency)
        {
            CurrencyDto dto = new CurrencyDto();
            dto.Id = id;
            dto.Name = currency.Name;
            dto.Code = currency.Code;
            dto.Symbol = currency.Symbol;
            var result = await _service.UpdateCurrency(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));
            }
            return dto;
        }
        public async Task<CurrencyDto> UpdateCurrencyStatusAsync(Guid id, LookUpStatusEnum status)
        {
            var result = await _service.UpdateCurrencyStatus(id, status);

            if (result)
                return await _service.GetCurrencyById(id);

            throw new GraphQLException(new Error("Attribute not found", "404"));
        }

        public async Task<bool> DeleteCurrencyAsync(Guid id)
        {
            var result = await _service.DeleteCurrency(id);

            if(result)
            return result;

            throw new GraphQLException(new Error("Attribute not found", "404"));
        }
    }
}
