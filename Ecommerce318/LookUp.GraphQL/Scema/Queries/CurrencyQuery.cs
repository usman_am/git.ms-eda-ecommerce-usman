﻿using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Scema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class CurrencyQuery
    {
        private readonly ICurrencyServices _service;

        public CurrencyQuery(ICurrencyServices service)
        {
            _service = service;
        }

        public async Task<IEnumerable<CurrencyDto>> GetAllCurrencyAsync()
        {
            IEnumerable<CurrencyDto> result = await _service.All();
            return result;
        }

        public async Task<CurrencyDto> GetCurrencyByIdAsync(Guid id)
        {
            return await _service.GetCurrencyById(id);
        }
    }
}
