﻿using AutoMapper;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Scema.Queries
{
    [ExtendObjectType(Name ="Query")]
    public class AttributeQuery
    {
        private readonly IAttributeService _service;

        public AttributeQuery(IAttributeService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<AttributeDto>> GetAllAttributeAsync()
        {
            IEnumerable<AttributeDto> result = await _service.All();
            return result;
        }

        public async Task<AttributeDto> GetAttributeByIdAsync(Guid id)
        {
            return await _service.GetAttributeById(id);
        }

    }
}
