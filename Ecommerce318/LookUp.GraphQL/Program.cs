using LookUp.GraphQL.Scema.Queries;
using LookUp.Domain;
using Microsoft.EntityFrameworkCore;
using LookUp.Domain.MapProfile;
using LookUp.Domain.Repositories;
using LookUp.Domain.Services;
using Framework.Core.Events.Externals;
using Framework.Kafka.Producers;
using Framework.Kafka;
using LookUp.GraphQL.Scema.Mutations;

var builder = WebApplication.CreateBuilder(args);
//tambah
builder.Services.AddDomainContext(option =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    option.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection
        ("LookUp_Db_Conn").Value);

    option.EnableSensitiveDataLogging(false)
    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});
//-------------

// Add services to the container.

builder.Services.AddControllers();

//tambah
builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
    config.AddProfile<EntityToDtoCurrency>();
});

builder.Services.AddKafkaProducer();
//------------------------------

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//tambahan juga
builder.Services
    .AddScoped<Query>()
    .AddScoped<AttributeQuery>()
    .AddScoped<CurrencyQuery>()
    .AddScoped<Mutation>()
    .AddScoped<AttributeMutation>()
    .AddScoped<CurrencyMutation>()
    .AddScoped<IAttributeRepository, AttributeRepository>()
    .AddScoped<IAttributeService, AttributeService>()
    .AddScoped<ICurrencyRepository, CurrencyRepository>()
    .AddScoped<ICurrencyServices, CurrencyService>()
    .AddGraphQLServer()
    .AddQueryType<Query>()
    .AddTypeExtension<AttributeQuery>()
    .AddTypeExtension<CurrencyQuery>()
    .AddMutationType<Mutation>()
    .AddTypeExtension<AttributeMutation>()
    .AddTypeExtension<CurrencyMutation>();

//-----------------------

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

//tambahan
app.MapGraphQL();
//----------------

app.Run();
