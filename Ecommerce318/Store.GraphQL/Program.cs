using Framework.Kafka;
using Framework.Core.Events;
using Microsoft.EntityFrameworkCore;
using Store.Domain;
using Store.Domain.MapProfile;
using Store.Domain.Repositories;
using Store.Domain.Services;
using Store.GraphQL.Schema.Queries;
using Store.GraphQL.Schema.Mutations;
using Microsoft.Extensions.FileProviders;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDomainContext(option =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    option.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection
        ("Store_Db_Conn").Value);

    option.EnableSensitiveDataLogging(false)
    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

builder.Services
    .AddScoped<ICategoryRepository, CategoryRepository>()
    .AddScoped<ICategoryService, CategoryService>();
builder.Services
    .AddScoped<IProductRepository, ProductRepository>()
    .AddScoped<IProductService, ProductService>();
builder.Services
    .AddScoped<IAttributeRepository, AttributeRepository>()
    .AddScoped<IAttributeService, AttributeService>();

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddStore();

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoAttribute>();
    config.AddProfile<EntityToDtoProduct>();
    config.AddProfile<EntityToDtoProfile>();
    config.AddProfile<EntityToDtoGallery>();
});

builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();
//builder.Services.AddKafkaProducer();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services
    .AddScoped<Query>()
    .AddScoped<ProductQuery>()
    .AddScoped<CategoryQuery>()
    .AddScoped<AttributeQuery>()
    .AddScoped<GalleryQuery>()
    .AddScoped<Mutation>()
    .AddScoped<ProductMutation>()
    .AddScoped<CategoryMutation>()
    .AddScoped<GalleryMutation>()///gallery upload file
    .AddScoped<IProductRepository, ProductRepository>()
    .AddScoped<IProductService, ProductService>()
    .AddScoped<ICategoryRepository, CategoryRepository>()
    .AddScoped<ICategoryService, CategoryService>()
    .AddScoped<IAttributeRepository, AttributeRepository>()
    .AddScoped<IAttributeService, AttributeService>()
    .AddScoped<IGalleryRepository, GalleryRepository>()///gallery upload file
    .AddScoped<IGalleryService, GalleryService>()///gallery upload file
    .AddGraphQLServer()
    .AddType<UploadType>()///gallery upload file
    .AddQueryType<Query>()
    .AddTypeExtension<ProductQuery>()
    .AddTypeExtension<CategoryQuery>()
    .AddTypeExtension<AttributeQuery>()
    .AddTypeExtension<GalleryQuery>()
    .AddMutationType<Mutation>()
    .AddTypeExtension<ProductMutation>()
    .AddTypeExtension<CategoryMutation>()
    .AddTypeExtension<GalleryMutation>();///gallery upload file

//builder.Services.AddKafkaProducerAndConsumer();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGraphQL();

//adding for file upload 14/06/2023
app.UseFileServer(new FileServerOptions()
{
    FileProvider = new PhysicalFileProvider(
        System.IO.Path.Combine(Directory.GetCurrentDirectory(), @"Resourses/Images")),
    RequestPath = new PathString("/image"),
    EnableDirectoryBrowsing = true
});
//-------------------

app.Run();
