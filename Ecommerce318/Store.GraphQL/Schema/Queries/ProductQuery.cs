﻿using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class ProductQuery
    {
        private readonly IProductService _service;

        public ProductQuery(IProductService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<ProductDto>> GetAllProductAsync()
        {
            IEnumerable<ProductDto> result = await _service.All();
            return result;
        }

        public async Task<ProductDto> GetProductByIdAsync(Guid id)
        {
            return await _service.GetProductById(id);
        }
    }
}
