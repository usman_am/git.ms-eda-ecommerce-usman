﻿using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class GalleryQuery
    {
        private readonly IGalleryService _service;

        public GalleryQuery(IGalleryService service)
        {
            _service = service;
        }

        /*public async Task<IEnumerable<GalleryDto>> GetAllGalleryAsync()
        {
            IEnumerable<GalleryDto> result = await _service.All();
            return result;
        }*/

        public async Task<GalleryDto> GetGalleryByIdAsync(Guid id)
        {
            return await _service.GetGalleryById(id);
        }
    }
}
