﻿using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Repositories;
using Store.Domain.Services;

namespace Store.GraphQL.Schema.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class ProductMutation
    {
       

        private readonly IProductService _service;

        public ProductMutation(IProductService service)
        {
            _service = service;
        }

        public async Task<ProductDto> AddProductAsync(ProductTypeInput product)
        {
            ProductDto dto = new ProductDto();
            dto.CategoryId = product.CategoryId;
            dto.AttributeId = product.AttributeId;
            dto.SKU = product.SKU;
            dto.Name = product.Name;
            dto.Description = product.Description;
            dto.Price = product.Price;
            dto.Volume = product.Volume;
            dto.Sold = product.Sold;
            dto.Stock = product.Stock;
            var result = await _service.AddProduct(dto);
            return result;
        }

        public async Task<ProductDto> EditProductAsync(Guid id, ProductTypeInput product)
        {
            ProductDto dto = new ProductDto();
            dto.Id = id;
            dto.CategoryId = product.CategoryId;
            dto.AttributeId = product.AttributeId;
            dto.SKU = product.SKU;
            dto.Name = product.Name;
            dto.Description = product.Description;
            dto.Price = product.Price;
            dto.Volume = product.Volume;
            dto.Sold = product.Sold;
            dto.Stock = product.Stock;
            var result = await _service.UpdateProduct(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));
            }
            return dto;
        }
        public async Task<ProductDto> UpdateProductStatusAsync(Guid id, StoreStatusEnum status)
        {
            var result = await _service.UpdateProductStatus(id, status);

            if (result)
                return await _service.GetProductById(id);

            throw new GraphQLException(new Error("Attribute not found", "404"));
        }

        public async Task<bool> DeleteProductAsync(Guid id)
        {
            var result = await _service.DeleteProduct(id);

            if (result)
                return result;

            throw new GraphQLException(new Error("Attribute not found", "404"));
        }
    }
}
