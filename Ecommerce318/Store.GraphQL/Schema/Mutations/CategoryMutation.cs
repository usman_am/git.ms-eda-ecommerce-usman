﻿using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.GraphQL.Schema.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class CategoryMutation
    {
        private readonly ICategoryService _service;

        public CategoryMutation(ICategoryService service)
        {
            _service = service;
        }

        public async Task<CategoryDto> AddCategoryAsync(CategoryTypeInput category)
        {
            CategoryDto dto = new CategoryDto();
            dto.Name = category.Name;
            dto.Description = category.Description;
            var result = await _service.AddCategory(dto);
            return result;
        }

        public async Task<CategoryDto> EditCategoryAsync(Guid id, CategoryTypeInput category)
        {
            CategoryDto dto = new CategoryDto();
            dto.Id = id;
            dto.Name = category.Name;
            dto.Description = category.Description;
            var result = await _service.UpdateCategory(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));
            }
            return dto;
        }
        public async Task<CategoryDto> UpdateCategoryStatusAsync(Guid id, StoreStatusEnum status)
        {
            var result = await _service.UpdateCategoryStatus(id, status);

            if (result)
                return await _service.GetCategoryById(id);

            throw new GraphQLException(new Error("Attribute not found", "404"));
        }

        public async Task<bool> DeleteCategoryAsync(Guid id)
        {
            var result = await _service.DeleteCategory(id);

            if (result)
                return result;

            throw new GraphQLException(new Error("Attribute not found", "404"));
        }
    }
}
