﻿using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Services;
using Path = System.IO.Path;

namespace Store.GraphQL.Schema.Mutations
{
    [ExtendObjectType(typeof(Mutation))]
    public class GalleryMutation
    {
        public string Introduction => "Hello, this is GraphQL Store mutation";
        private readonly IGalleryService _service;
        private string[] acceptedExt = new string[] { ".jpg", ".jpeg", ".png", ".gif" };

        public GalleryMutation(IGalleryService service)
        {
            _service = service;
        }


        public async Task<IEnumerable<GalleryDto>> GetAllGalleryAsync()
        {
            IEnumerable<GalleryDto> result = await _service.All();
            return result;
        }

        public async Task<GalleryDto> AddGalleryAsync(GalleryTypeInput gallery)
        {
            try
            {
                GalleryDto dto = new GalleryDto();
                dto.Name = gallery.Name;
                dto.Description = gallery.Description;
                dto.Status = StoreStatusEnum.Active;

                string fileExt = Path.GetExtension(gallery.File.Name);
                if (Array.IndexOf(acceptedExt, fileExt) != -1)
                {
                    var uniqueFileName = GetUniqueName(gallery.File.Name);

                    var uploads = Path.Combine("Resourses", "Images");
                    var filePath = Path.Combine(uploads, uniqueFileName);

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await gallery.File.CopyToAsync(fileStream);
                    }

                    dto.FileLink = uniqueFileName;
                }

                return await _service.AddGallery(dto);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private string GetUniqueName(string fileName)
        {
            fileName = Path.GetFileName(fileName);

            return Path.GetFileNameWithoutExtension(fileName)
                + "_" + Guid.NewGuid().ToString().Substring(0, 4)
                + Path.GetExtension(fileName);
        }
    }
}
