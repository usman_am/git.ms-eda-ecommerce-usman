﻿using AutoMapper;
using Framework.Auth;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using User.Domain.Dtos;
using User.Domain.Entities;
using User.Domain.EventEnvelopes.User;
using User.Domain.Repositories;

namespace User.Domain.Services
{
    public interface IUserService
    {
        Task<LoginDto> Login(string userName, string password);
        Task<IEnumerable<UserDto>> All();
        Task<UserDto> GetUserById(Guid id);
        Task<UserDto> AddUser(UserDto dto);
        Task<bool> UpdateUser(UserDto dto);
        Task<bool> UpdateUserStatus(Guid id, UserStatusEnum status);
        Task<bool> DeleteUser(Guid id);
    }
    public class UserService : IUserService
    {
        private IUserRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public UserService(IUserRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<IEnumerable<UserDto>> All()
        {
            return _mapper.Map<IEnumerable<UserDto>>(await _repository.GetAll());
        }

        public async Task<UserDto> GetUserById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null && result.Status != UserStatusEnum.Removed)
                {
                    return _mapper.Map<UserDto>(result);
                }
            }
            return null;
        }

        public async Task<LoginDto> Login(string userName, string password)
        {
            var entity = await _repository.Login(userName, password);
            if (entity != null)
            {
                LoginDto dto = _mapper.Map<LoginDto>(entity);
                dto.Roles.Add(entity.Type.ToString());
                return dto;
            }
            return null;
        }

        public async Task<UserDto> AddUser(UserDto dto)
        {
            if (dto != null)
            {
                dto.Status = UserStatusEnum.Inactive;
                dto.Password = Encryption.HashSha256(dto.Password);
                var dtoToEntity = _mapper.Map<UserEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsyc();

                if (result > 0)
                {
                    var externalEvent = new EventEnvelope<UserCreated>(
                   UserCreated.Create(
                       entity.Id,
                       entity.UserName,
                       entity.Password,
                       entity.FirstName,
                       entity.LastName,
                       entity.Email,
                       entity.Type,
                       entity.Status,
                       entity.Modified
                   )
                   );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return _mapper.Map<UserDto>(entity);
                }
            }
            return new UserDto();
        }

        public async Task<bool> UpdateUser(UserDto dto)
        {
            if (dto != null)
            {
                var attribute = await _repository.GetById(dto.Id);
                if (attribute != null && attribute.Status != UserStatusEnum.Removed)
                {
                    dto.Status = attribute.Status;
                    var entity = await _repository.Update(_mapper.Map<UserEntity>(dto));
                    var result = await _repository.SaveChangesAsyc();
                    var externalEvent = new EventEnvelope<UserUpdated>(
                    UserUpdated.Update(
                       entity.Id,
                       entity.UserName,
                       entity.Password,
                       entity.FirstName,
                       entity.LastName,
                       entity.Email,
                       entity.Type,
                       entity.Modified
                    )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    if (result > 0) return true;
                }
            }
            return false;
        }

        public async Task<bool> UpdateUserStatus(Guid id, UserStatusEnum status)
        {
            if (id != null && status != null)
            {
                var attribute = await _repository.GetById(id);
                if (attribute != null)
                {
                    attribute.Status = status;
                    var entity = await _repository.Update(_mapper.Map<UserEntity>(attribute));
                    var result = await _repository.SaveChangesAsyc();
                    var externalEvent = new EventEnvelope<UserStatus>(
                    UserStatus.EditStatus(
                       entity.Id,
                       entity.Status,
                       entity.Modified
                    )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    if (result > 0) return true;
                }
            }
            return false;
        }

        public async Task<bool> DeleteUser(Guid id)
        {
            var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.Status = UserStatusEnum.Removed;
                var entity = await _repository.Update(attribute);
                var result = await _repository.SaveChangesAsyc();
                var externalEvent = new EventEnvelope<UserDeleted>(
                   UserDeleted.Delete(
                       entity.Id,
                       entity.Status
                   )
                   );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                if (result > 0) return true;
            }
            return false;
        }
    }
}
