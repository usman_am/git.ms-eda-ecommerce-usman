﻿
using AutoMapper;
using User.Domain.Dtos;
using User.Domain.Entities;

namespace User.Domain.MapProfile
{
    public class EntityToDtoUser : Profile
    {
        public EntityToDtoUser() : base("User to Dto profile")
        {
            CreateMap<UserEntity, UserDto>();
            CreateMap<UserDto, UserEntity>();
            CreateMap<UserEntity, LoginDto>();
            CreateMap<LoginDto, UserEntity>();
        }
    }
}
