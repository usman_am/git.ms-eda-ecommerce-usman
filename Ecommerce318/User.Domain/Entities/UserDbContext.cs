﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using User.Domain.Entities.Configurations;

namespace User.Domain.Entities
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> options) : base(options) { }
        
        public DbSet<UserEntity> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
    }
}
