﻿using Framework.Auth;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User.Domain.Entities;

namespace User.Domain.Repositories
{
    public interface IUserRepository
    {
        Task<UserEntity> Login(string userName, string password);
        Task<Attribute> GetCount();
        Task<IEnumerable<UserEntity>> GetAll();
        Task<IEnumerable<UserEntity>> GetPaged(int page, int size);
        Task<UserEntity> GetById(Guid id);
        Task<UserEntity> Add(UserEntity entity);
        Task<UserEntity> Update(UserEntity entity);
        void Delete(UserEntity entity);
        Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default);
    }
    public class UserRepository : IUserRepository
    {
        protected readonly UserDbContext _context;
        public UserRepository(UserDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<UserEntity> Login(string userName, string password)
        {
            return await _context.Set<UserEntity>()
                .FirstOrDefaultAsync(o => o.UserName == userName && o.Password == Encryption.HashSha256(password));
        }

        public async Task<UserEntity> Add(UserEntity entity)
        {
            _context.Set<UserEntity>().Add(entity);
            return entity;
        }

        public void Delete(UserEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<UserEntity>> GetAll()
        {
            return await _context.Set<UserEntity>()
               .Where(u => u.Status != UserStatusEnum.Removed).ToListAsync();
        }

        public async Task<UserEntity> GetById(Guid id)
        {
            return await _context.Set<UserEntity>().FindAsync(id);
        }

        public Task<Attribute> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<UserEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<UserEntity> Update(UserEntity entity)
        {
            _context.Set<UserEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        
    }

    
}
