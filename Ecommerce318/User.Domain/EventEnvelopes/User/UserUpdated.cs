﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User.Domain.EventEnvelopes.User
{
    public record UserUpdated(
        Guid Id,
        string UserName,
        string Password,
        string FirstName,
        string LastName,
        string Email,
        UserTypeEnum Type,
        DateTime Modified)
    {
        public static UserUpdated Update(
            Guid id,
            string userName,
            string password,
            string firstName,
            string lastName,
            string email,
            UserTypeEnum type,
            DateTime modified
            ) => new(id, userName, password, firstName, lastName, email, type, modified);
    }
}