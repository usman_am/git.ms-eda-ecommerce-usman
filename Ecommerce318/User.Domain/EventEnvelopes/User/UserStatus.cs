﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User.Domain.EventEnvelopes.User
{
    public record UserStatus(
        Guid Id,
        UserStatusEnum Status,
        DateTime Modified)
    {
        public static UserStatus EditStatus(
            Guid id,
            UserStatusEnum status,
            DateTime modified
            ) => new(id, status, modified);
    }
}
