﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User.Domain.EventEnvelopes.User
{
    public record UserDeleted(
        Guid Id,
        UserStatusEnum Status)
    {
        public static UserDeleted Delete(
            Guid id,
            UserStatusEnum status
            ) => new(id, status);
    }
}
