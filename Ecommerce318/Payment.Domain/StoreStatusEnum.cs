﻿
using System.Text.Json.Serialization;

namespace Payment.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum StoreStatusEnum //ganti ke public enum
    {
        Active,
        Inactive,
        Removed
    }
}
