﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Repositories
{
    public interface IPaymentRepository
    {
        Task<Attribute> GetCount();
        Task<IEnumerable<PaymentEntity>> GetAll();
        Task<IEnumerable<PaymentEntity>> GetPaged(int page, int size);
        Task<PaymentEntity> GetById(Guid id);
        //Task<PaymentEntity> Add(PaymentEntity entity);
        Task<PaymentEntity> Update(PaymentEntity entity);
        void Delete(PaymentEntity entity);
        Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default);
    }
    public class PaymentRepository : IPaymentRepository
    {
        protected readonly PaymentDbContext _context;

        public PaymentRepository(PaymentDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        /*public async Task<PaymentEntity> Add(PaymentEntity entity)
        {
            _context.Set<PaymentEntity>().Add(entity);
            return entity;
        }*/

        public void Delete(PaymentEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<PaymentEntity>> GetAll()
        {
            return await _context.Set<PaymentEntity>().ToListAsync();
        }

        public async Task<PaymentEntity> GetById(Guid id)
        {
            return await _context.Set<PaymentEntity>().FindAsync(id);
        }

        public Task<Attribute> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<PaymentEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<PaymentEntity> Update(PaymentEntity entity)
        {
            _context.Set<PaymentEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
