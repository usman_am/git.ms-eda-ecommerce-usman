﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;

namespace Payment.Domain.Repositories
{
    public interface ICartRepository
    {
        Task<IEnumerable<CartEntity>> GetAll();
        Task<CartEntity> GetById(Guid id);
        Task<CartEntity> Update(CartEntity entity);
        Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default);
    }
    public class CartRepository : ICartRepository
    {
        protected readonly PaymentDbContext _context;

        public CartRepository(PaymentDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<CartEntity>> GetAll()
        {
            return await _context.Set<CartEntity>().Include("Customer").ToListAsync();
        }

        public async Task<CartEntity> GetById(Guid id)
        {
            return await _context.Set<CartEntity>().FindAsync(id);
        }

        public async Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<CartEntity> Update(CartEntity entity)
        {
            _context.Set<CartEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    
}
