﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace Payment.Domain.Entities
{
    public class PaymentEntity
    {
        public Guid Id { get; set; }
        public Guid CartId { get; set; }
        public decimal Total { get; set; } = default!;
        public decimal Pay { get; set; } = default!;
        public CartStatusEnum Status { get; set; } = default!;
        public PaymentStatusEnum PaymentStatus { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;
        [ForeignKey("CartId")]
        public virtual CartEntity Cart { get; set; }
    }
}
