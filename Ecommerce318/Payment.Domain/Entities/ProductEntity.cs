﻿namespace Payment.Domain.Entities
{
    public class ProductEntity
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public Guid AttributeId { get; set; }
        public string SKU { get; set; } = default!;
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
        public decimal Price { get; set; } = default!;
        public decimal Volume { get; set; } = default!;
        public int Sold { get; set; } = default!;
        public int Stock { get; set; } = default!;
        public StoreStatusEnum Status { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;
    }
}
