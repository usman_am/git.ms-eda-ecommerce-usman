﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace Payment.Domain.Entities.Configurations
{
    public class CartConfiguration : IEntityTypeConfiguration<CartEntity>
    {
        public void Configure(EntityTypeBuilder<CartEntity> builder)
        {
            builder.ToTable("Carts");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.CustomerId).IsRequired();
            builder.Property(e => e.Total).HasPrecision(18, 2);
            builder.Property(e => e.Pay).HasPrecision(18, 2);
            builder.Property(e => e.Status).IsRequired();
        }
    }

    public class CartProductConfiguration : IEntityTypeConfiguration<CartProductEntity>
    {
        public void Configure(EntityTypeBuilder<CartProductEntity> builder)
        {
            builder.ToTable("CartProducts");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).IsRequired();
            builder.Property(x => x.CartId).IsRequired();
            builder.Property(x => x.ProductId).IsRequired();
            builder.Property(x => x.SKU).IsRequired();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.Quatity).IsRequired();
            builder.Property(x => x.Price).HasPrecision(18, 2).IsRequired();
        }
    }

    public class PaymentConfiguration : IEntityTypeConfiguration<PaymentEntity>
    {
        public void Configure(EntityTypeBuilder<PaymentEntity> builder)
        {
            builder.ToTable("Payments");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.CartId).IsRequired();
            builder.Property(e => e.Total).HasPrecision(18, 2).IsRequired();
            builder.Property(e => e.Pay).HasPrecision(18, 2).IsRequired();
            builder.Property(e => e.Status).IsRequired();
            builder.Property(e => e.PaymentStatus).IsRequired();
        }
    }

    public class ProductConfiguration : IEntityTypeConfiguration<ProductEntity>
    {
        public void Configure(EntityTypeBuilder<ProductEntity> builder)
        {
            builder.ToTable("Products");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.CategoryId).IsRequired();
            builder.Property(e => e.AttributeId).IsRequired();
            builder.Property(e => e.SKU).IsRequired();
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.Description).IsRequired();
            builder.Property(e => e.Price).HasPrecision(18, 2).IsRequired();
            builder.Property(e => e.Volume).HasPrecision(18, 2).IsRequired();
            builder.Property(e => e.Sold).IsRequired();
            builder.Property(e => e.Stock).IsRequired();
            builder.Property(e => e.Status).IsRequired();
        }
    }

    public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).IsRequired();
            builder.Property(x => x.UserName).HasMaxLength(50).IsRequired();
            builder.Property(x => x.Password).IsRequired();
            builder.Property(x => x.FirstName).IsRequired();
            builder.Property(x => x.LastName).IsRequired(false);
            builder.Property(x => x.Email).IsRequired();
            builder.Property(x => x.Type).IsRequired();
            builder.Property(x => x.Status).IsRequired();
        }
    }
}