﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Dtos
{
    public class PaymentDto
    {
        public Guid Id { get; set; }
        public Guid CartId { get; set; }
        public decimal Total { get; set; } 
        public decimal Pay { get; set; } 
        public CartStatusEnum Status { get; set; } 
        public PaymentStatusEnum PaymentStatus { get; set; } 
    }
}
