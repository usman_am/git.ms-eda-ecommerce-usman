﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Dtos
{
    public class CartProductDto
    {
        public Guid Id { get; set; }
        public Guid CartId { get; set; }
        public Guid ProductId { get; set; }
        public string SKU { get; set; }
        public string Name { get; set; }
        public int Quatity { get; set; }
        public decimal Price { get; set; }
    }
}
