﻿using Framework.Core.Events;
using Payment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Projections
{
    public record UserCreated(
        Guid Id,
        string UserName,
        string Password,
        string FirstName,
        string LastName,
        string Email,
        UserTypeEnum Type,
        ProductStatusEnum Status,
        DateTime Modified
    );

    public record UserUpdated(
        Guid Id,
        string UserName,
        string Password,
        string FirstName,
        string LastName,
        string Email,
        UserTypeEnum Type,
        DateTime Modified
    );

    public record UserStatus(
        Guid? Id,
        ProductStatusEnum Status,
        DateTime Modified
    );

    public record UserDeleted(
        Guid? Id,
        ProductStatusEnum Status,
        DateTime Modified
    );

    public class UserProjection
    {
        public static bool Handle(EventEnvelope<UserCreated> eventEnvelope)
        {
            var (id, userName,password, firstName, lastName, email,type,status, modified) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                UserEntity entity = new UserEntity()
                {
                    Id = (Guid)id,
                    UserName = userName,
                    Password = password,
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email,
                    Type = type,
                    Status = status,
                    Modified = modified
                };
                entity.Status = ProductStatusEnum.Inactive;

                context.Users.Add(entity);
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<UserUpdated> eventEnvelope)
        {
            var (id, userName, password, firstName, lastName, email, type, modified) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);
                entity.UserName = userName;
                entity.Password = password;
                entity.FirstName = firstName;
                entity.LastName = lastName;
                entity.Email = email;
                entity.Type = type;
                entity.Modified = modified;
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<UserStatus> eventEnvelope)
        {
            var (id, status, modified) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);
                entity.Status = status;
                entity.Modified = modified;
                context.SaveChanges();
            }
            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<UserDeleted> eventEnvelope)
        {
            var (id, status, modified) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);
                entity.Status = status;
                entity.Modified = modified;
                context.SaveChanges();
            }
            return true; //new AttributeCreated(id, type, unit, status, modified);
        }
    }
}
