﻿
using Framework.Core.Events;
using Payment.Domain.Entities;

namespace Payment.Domain.Projections
{
    public record CartCreated(
        Guid? Id,
        Guid CustomerId,
        CartStatusEnum Status,
        DateTime Modified
    );

    public record CartUpdated(
        Guid? Id,
        Guid CustomerId,
        DateTime Modified
    );

    public record CartStatus(
        Guid? Id,
        CartStatusEnum Status,
        List<CartProductItem> CartProducts,
        DateTime Modified
    );

    public class CartProductItem
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }

    public record CartDeleted(
        Guid? Id,
        CartStatusEnum Status,
        DateTime Modified
    );
    public class CartProjection
    {
        private readonly PaymentDbContext _context;


        public CartProjection()
        {
        }
        public static bool Handle(EventEnvelope<CartCreated> eventEnvelope)
        {
            var (id, customerId, status, modified) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                CartEntity entity = new CartEntity()
                {
                    Id = (Guid)id,
                    CustomerId = customerId,
                    Status = status,
                    Modified = modified
                };


                PaymentEntity entityPayment = new PaymentEntity()
                {
                    CartId = (Guid)id,
                    Total = 0,
                    Pay = 0,
                    Status = status,
                    PaymentStatus = PaymentStatusEnum.Pending
                };

                context.Carts.Add(entity);
                context.Payments.Add(entityPayment);
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<CartUpdated> eventEnvelope)
        {
            var (id, customerId, modified) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                CartEntity entity = context.Set<CartEntity>().Find(id);
                entity.CustomerId = customerId;
                entity.Modified = modified;
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<CartStatus> eventEnvelope)
        {
            var (id, status, cartProducts, modified) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                //CartEntity entity = context.Set<CartEntity>().Find(id);
                CartEntity entity = context.Carts.Where(o => o.Id == id).FirstOrDefault();
  
                entity.Status = status;
                entity.Modified = modified;

                decimal total = 0;
                foreach (var item in cartProducts)
                {
                    ProductEntity prod = context.Products.Where(o => o.Id == item.ProductId).FirstOrDefault();
                    if(prod != null)
                    {
                        total += item.Quantity * prod.Price;

                        CartProductEntity cartProd = new CartProductEntity()
                        {
                            Id = (Guid)item.Id,
                            CartId = (Guid)id,
                            ProductId = item.ProductId,
                            Price = item.Price,
                            Quatity = item.Quantity
                        };
                        context.CartProducts.Add(cartProd);
                    }
                    entity.Total = total;
                }

                PaymentEntity entityPayment = context.Payments.Where(o => o.CartId == id).FirstOrDefault();
                entityPayment.Status = status;
                if(entityPayment.Status == CartStatusEnum.Canceled) { entityPayment.PaymentStatus = PaymentStatusEnum.Cancel; }
                context.SaveChanges();
            }
            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<CartDeleted> eventEnvelope)
        {
            var (id, status, modified) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                PaymentEntity entity = context.Set<PaymentEntity>().Find(id);
                entity.Status = status;
                entity.Modified = modified;
                context.SaveChanges();
            }
            return true; //new AttributeCreated(id, type, unit, status, modified);
        }
    }
}
