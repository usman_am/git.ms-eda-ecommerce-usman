﻿using Framework.Core.Events;
using Payment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Projections
{
    public record CartProductCreated(
        Guid Id,
        Guid CartId,
        Guid ProductId,
        string SKU,
        string Name,
        int Quantity,
        decimal Price,
        DateTime Modified
    );

    public record CartProductUpdated(
        Guid Id,
        Guid CartId,
        Guid ProductId,
        string SKU,
        string Name,
        int Quantity,
        decimal Price,
        DateTime Modified
    );

    public class CartProductProjection
    {
        private readonly PaymentDbContext _context;

        public CartProductProjection()
        {
            
        }

        public static bool Handle(EventEnvelope<CartProductCreated> eventEnvelope)
        {
            var (id, cartId, productId, sKU, name, quantity, price, modified) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                CartProductEntity entity = new CartProductEntity()
                {
                    Id = (Guid)id,
                    CartId = cartId,
                    ProductId = productId,
                    SKU = sKU,
                    Name = name,
                    Quatity = quantity,
                    Price = price,
                    Modified = modified
                };

                context.CartProducts.Add(entity);
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<CartProductUpdated> eventEnvelope)
        {
            var (id, cartId, productId, sKU, name, quantity, price, modified) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                CartProductEntity entity = context.Set<CartProductEntity>().Find(id);
                entity.CartId = cartId;
                entity.ProductId = productId;
                entity.SKU= sKU;
                entity.Name = name;
                entity.Quatity = quantity;
                entity.Price = price;
                entity.Modified = modified;
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }
    }
}
