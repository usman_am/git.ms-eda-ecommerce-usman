﻿using AutoMapper;
using Payment.Domain.Dtos;
using Payment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.MapProfile
{
    public class EntityToDtoPayment : Profile
    {
        public EntityToDtoPayment(): base("Entity to Dto Payment")
        {
            CreateMap<PaymentEntity, PaymentDto>();
            CreateMap<PaymentDto, PaymentEntity>();
        }
    }
}
