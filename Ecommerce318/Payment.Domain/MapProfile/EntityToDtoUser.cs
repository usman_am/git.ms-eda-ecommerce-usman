﻿using AutoMapper;
using Payment.Domain.Dtos;
using Payment.Domain.Entities;

namespace Payment.Domain.MapProfile
{
    public class EntityToDtoUser : Profile
    {
        public EntityToDtoUser() : base("Entity to Dto user")
        {
            CreateMap<UserEntity, UserDto>();
        }
    }
}
