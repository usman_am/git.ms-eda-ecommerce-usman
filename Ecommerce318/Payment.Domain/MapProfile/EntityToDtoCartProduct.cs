﻿using AutoMapper;
using Payment.Domain.Dtos;
using Payment.Domain.Entities;
using Payment.Domain.EventEnvelopes;

namespace Payment.Domain.MapProfile
{
    public class EntityToDtoCartProduct : Profile
    {
        public EntityToDtoCartProduct()
        {
            CreateMap<CartProductEntity, CartProductDto>();
            CreateMap<CartProductEntity, CartProductItem>();
        }
    }
}
