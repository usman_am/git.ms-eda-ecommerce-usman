﻿using AutoMapper;
using Payment.Domain.Dtos;
using Payment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.MapProfile
{
    public class EntityToDtoCart : Profile
    {
        public EntityToDtoCart() : base("Entity to Dto Cart")
        {
            CreateMap<CartEntity, CartDto>();
            CreateMap<CartDto, CartEntity>();
        }
    }
}
