﻿using AutoMapper;
using Payment.Domain.Dtos;
using Payment.Domain.Entities;
using Payment.Domain.Repositories;

namespace Payment.Domain.Services
{
    public interface IPaymentService
    {
        Task<IEnumerable<PaymentDto>> All();
        Task<PaymentDto> GetPaymentById(Guid id);
        //Task<PaymentDto> AddPayment(PaymentDto dto);
        Task<bool> UpdatePayment(PaymentDto dto);
        Task<bool> UpdatePaymentStatus(Guid id, PaymentStatusEnum status);
        Task<bool> DeletePayment(Guid id);
    }
    public class PaymentService : IPaymentService
    {
        private IPaymentRepository _repository;
        private readonly IMapper _mapper;

        public PaymentService(IPaymentRepository repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<PaymentDto>> All()
        {
            return _mapper.Map<IEnumerable<PaymentDto>>(await _repository.GetAll());
        }

        public async Task<PaymentDto> GetPaymentById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<PaymentDto>(result);
                }
            }
            return null;
        }

       /* public async Task<PaymentDto> AddPayment(PaymentDto dto)
        {
            if (dto != null)
            {
                dto.Status = CartStatusEnum.Confirmed;
                var dtoToEntity = _mapper.Map<PaymentEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsyc();

                if (result > 0)
                    return _mapper.Map<PaymentDto>(entity);
            }
            return new PaymentDto();
        }*/

        public async Task<bool> UpdatePayment(PaymentDto dto)
        {
            if (dto != null)
            {
                var attribute = await _repository.GetById(dto.Id);
                if (attribute != null)
                {
                    var entity = await _repository.Update(_mapper.Map<PaymentEntity>(dto));
                    var result = await _repository.SaveChangesAsyc();

                    if (result > 0) return true;
                }
            }
            return false;
        }

        public async Task<bool> DeletePayment(Guid id)
        {
            var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.Status = CartStatusEnum.Canceled;
                attribute.PaymentStatus = PaymentStatusEnum.Cancel;
                var entity = await _repository.Update(attribute);
                var result = await _repository.SaveChangesAsyc();

                if (result > 0) return true;
            }

            return false;
        }

        public async Task<bool> UpdatePaymentStatus(Guid id, PaymentStatusEnum status)
        {
            var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.PaymentStatus = status;
                var entity = await _repository.Update(attribute);
                var result = await _repository.SaveChangesAsyc();
                if (result > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }

    
}
