﻿using AutoMapper;
using Payment.Domain.Dtos;
using Payment.Domain.Entities;
using Payment.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Services
{
    public interface ICartService
    {
        Task<IEnumerable<CartDto>> All();
        Task<CartDto> GetCartById(Guid id);
        //Task<CartDto> AddCart(CartDto dto);
        Task<bool> UpdateCart(CartDto dto);
        Task<bool> DeleteCart(Guid id);
    }
    public class CartService : ICartService
    {
        private ICartRepository _repository;
        private readonly IMapper _mapper;

        public CartService(ICartRepository repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CartDto>> All()
        {
            return _mapper.Map<IEnumerable<CartDto>>(await _repository.GetAll());
        }

        public async Task<CartDto> GetCartById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CartDto>(result);
                }
            }
            return null;
        }

       /* public async Task<CartDto> AddCart(CartDto dto)
        {
            if (dto != null)
            {
                dto.Status = CartStatusEnum.Confirmed;
                var dtoToEntity = _mapper.Map<CartEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsyc();

                if (result > 0)
                    return _mapper.Map<CartDto>(entity);
            }
            return new CartDto();
        }*/

        public async Task<bool> UpdateCart(CartDto dto)
        {
            if (dto != null)
            {
                var attribute = await _repository.GetById(dto.Id);
                if (attribute != null)
                {
                    var entity = await _repository.Update(_mapper.Map<CartEntity>(dto));
                    var result = await _repository.SaveChangesAsyc();

                    if (result > 0) return true;
                }
            }
            return false;
        }

        public async Task<bool> DeleteCart(Guid id)
        {
            var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.Status = CartStatusEnum.Canceled;
                var entity = await _repository.Update(attribute);
                var result = await _repository.SaveChangesAsyc();

                if (result > 0) return true;
            }

            return false;
        }
    }

   
}
