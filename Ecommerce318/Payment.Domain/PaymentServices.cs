﻿using Framework.Core.Projection;
using Microsoft.Extensions.DependencyInjection;
using Payment.Domain.Projections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain
{
    public static class PaymentServices
    {
        public static IServiceCollection addPayment(this IServiceCollection services) =>
            services.AddProjections();

        private static IServiceCollection AddProjections(this IServiceCollection services) =>
            services
            .Projection(builder => builder
            .AddOn<CartCreated>(CartProjection.Handle)
            .AddOn<CartUpdated>(CartProjection.Handle)
            .AddOn<CartStatus>(CartProjection.Handle)
            .AddOn<CartDeleted>(CartProjection.Handle)
            .AddOn<UserCreated>(UserProjection.Handle)
            .AddOn<UserUpdated>(UserProjection.Handle)
            .AddOn<UserStatus>(UserProjection.Handle)
            .AddOn<UserDeleted>(UserProjection.Handle)
            .AddOn<CartProductCreated>(CartProductProjection.Handle)
            .AddOn<CartProductUpdated>(CartProductProjection.Handle)
            .AddOn<ProductCreated>(ProductProjection.Handle)
            .AddOn<ProductUpdated>(ProductProjection.Handle)
            .AddOn<ProductStatusChanged>(ProductProjection.Handle)
            .AddOn<ProductDeleted>(ProductProjection.Handle));
    }
}
