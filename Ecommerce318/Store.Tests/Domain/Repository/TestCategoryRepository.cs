﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities;
using Store.Domain.Repositories;
using Store.Tests.MockData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Store.Tests.Domain.Repository
{
    public class TestCategoryRepository : IDisposable
    {
        protected readonly StoreDbContext _context;
        private readonly ITestOutputHelper _output;

        public TestCategoryRepository(ITestOutputHelper output)
        {
            var options = new DbContextOptionsBuilder<StoreDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            _context = new StoreDbContext(options);
            _context.Database.EnsureCreated();

            _output = output;
        }

        [Fact]
        public async Task GetAllAsync_ReturnCategoriesCollection()
        {
            // Arrange
            _context.Categories.AddRange(CategoryMockData.GetCategories());
            _context.SaveChanges();

            var repo = new CategoryRepository(_context);
            // Action
            var result = await repo.GetAll();
            var count = CategoryMockData.GetCategories().Count();
            _output.WriteLine("Count : {0}", count);

            // Assert
            result.Should().HaveCount(3);
        }

        [Fact]
        public async Task GetByIdAsync_ReturnCategoriesCollection()
        {
            // Arrange
            _context.Categories.AddRange(CategoryMockData.GetCategories());
            _context.SaveChanges();

            var repo = new CategoryRepository(_context);
            // Action
            var result = await repo.GetById(new Guid("4761DBAF-27CA-4BEF-AC8E-450FAFEB140B"));

            // Assert
            result.Name.Should().Be("Food");
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted(); //membuang data tidak terpakai
            _context.Dispose();
        }
    }
}
