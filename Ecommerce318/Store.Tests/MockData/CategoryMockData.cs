﻿using Store.Domain;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Tests.MockData
{
    public class CategoryMockData
    {
        public static List<CategoryEntity> GetCategories()
        {
            return new List<CategoryEntity>
            {
                new CategoryEntity
                {
                    Id = new Guid("4761DBAF-27CA-4BEF-AC8E-450FAFEB140B"),
                    Name = "Food",
                    Description = "Makanan pokok",
                    Status = StoreStatusEnum.Active
                },
                new CategoryEntity
                {
                    Id = new Guid("3114608C-C7B3-46D9-B804-3CB81B2A058B"),
                    Name = "Minuman",
                    Description = "Minuman ya minuman",
                    Status = StoreStatusEnum.Inactive
                },
                new CategoryEntity
                {
                    Id = new Guid("E5C46781-BAA6-4207-B927-F5611F80CA57"),
                    Name = "Sack",
                    Description = "Makanan ringan",
                    Status = StoreStatusEnum.Active
                }
            };
        }
    }
}
