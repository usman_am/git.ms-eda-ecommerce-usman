﻿using Microsoft.Extensions.DependencyInjection;

namespace Gateway.GraphQL.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public const string LookUps = "LookUpService";
        public const string Stores = "StoreService";
        public const string Users = "UserServices";
        public const string Carts = "CartServices";
        public const string Payments = "PaymentServices";
        public static IServiceCollection AddHttpClientServices(this IServiceCollection services)
        {   //tambahkan url dari graphql lookups tabel properties lauchsettings.json "applicationUrl"
            services.AddHttpClient(LookUps, c => c.BaseAddress = new Uri("https://localhost:7259/graphql"));
            services.AddHttpClient(Stores, c => c.BaseAddress = new Uri("https://localhost:7157/graphql"));
            services.AddHttpClient(Users, c => c.BaseAddress = new Uri("https://localhost:7027/graphql"));
            services.AddHttpClient(Carts, c => c.BaseAddress = new Uri("https://localhost:7182/graphql"));
            services.AddHttpClient(Payments, c => c.BaseAddress = new Uri("https://localhost:7045/graphql"));


            services
                .AddGraphQLServer()
                .AddRemoteSchema(LookUps)
                .AddRemoteSchema(Stores)
                .AddRemoteSchema(Users)
                .AddRemoteSchema(Carts)
                .AddRemoteSchema(Payments);

            return services;
        }
    }
}
