﻿using LookUp.Domain;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading;

namespace LookUp.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        private readonly ICurrencyServices _service;
        private readonly ILogger<CurrencyController> _logger;

        public CurrencyController(ICurrencyServices service, ILogger<CurrencyController> logger)
        {
            _service = service;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpGet("GetById")]
        public async Task<IActionResult> GetById(Guid id)
        {
            if (id != null)
            {
                var isGeted = await _service.GetCurrencyById(id);
                if (isGeted != null)
                    return Ok(isGeted);
            }
            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CurrencyDto payload,
            CancellationToken cancellationToken)
        {
            try
            {
                var dto = await _service.AddCurrency(payload);
                if (dto != null)
                    return Ok(dto);
            }
            catch (OperationCanceledException ex) when
            (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Put(Guid id, [FromBody] CurrencyDto payload,
           CancellationToken cancellationToken)
        {
            try
            {
                if (payload != null)
                {
                    payload.Id = id;
                    var isUpdated = await _service.UpdateCurrency(payload);
                    if (isUpdated)
                        return Ok(isUpdated);
                }
            }
            catch (OperationCanceledException ex) when
                (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }


        [HttpPut("status")]
        public async Task<IActionResult> Put(Guid id, LookUpStatusEnum status,
            CancellationToken cancellationToken)
        {
            try
            {
                if (status != null)
                {
                    var isUpdated = await _service.UpdateCurrencyStatus(id, status);
                    if (isUpdated)
                        return Ok(isUpdated);
                }
            }
            catch (OperationCanceledException ex) when
                (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id, CancellationToken cancellationToken)
        {
            try
            {
                if (id != null)
                {
                    var isDeleted = await _service.DeleteCurrency(id);
                    if (isDeleted)
                        return Ok(isDeleted);
                }
            }
            catch (OperationCanceledException ex) when
                (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }
    }
}
