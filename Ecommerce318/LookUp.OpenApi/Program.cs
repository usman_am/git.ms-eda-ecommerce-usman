using Framework.Kafka;
using LookUp.Domain;
using LookUp.Domain.MapProfile;
using LookUp.Domain.Repositories;
using LookUp.Domain.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDomainContext(option =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    option.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection
        ("LookUp_Db_Conn").Value);

    option.EnableSensitiveDataLogging(false)
    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

// Add services to the container.


builder.Services
    .AddScoped<IAttributeRepository, AttributeRepository>()
    .AddScoped<IAttributeService, AttributeService>();
builder.Services
    .AddScoped<ICurrencyRepository, CurrencyRepository>()
    .AddScoped<ICurrencyServices, CurrencyService>();

builder.Services.AddControllers();
builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
    config.AddProfile<EntityToDtoCurrency>();
});

builder.Services.AddKafkaProducer();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
