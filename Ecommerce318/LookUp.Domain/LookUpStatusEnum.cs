﻿using System.Text.Json.Serialization;

namespace LookUp.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum LookUpStatusEnum //ganti ke public enum
    {
        Active,
        Inactive,
        Removed
    }
}
