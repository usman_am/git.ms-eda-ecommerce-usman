﻿using LookUp.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace LookUp.Domain.Repositories
{
    public interface IAttributeRepository
    {
        Task<Attribute> GetCount();
        Task<IEnumerable<AttributeEntity>> GetAll();
        Task<IEnumerable<AttributeEntity>> GetPaged(int page, int size);
        Task<AttributeEntity> GetById(Guid id);
        Task<AttributeEntity> Add(AttributeEntity entity);
        Task<AttributeEntity> Update(AttributeEntity entity);
        void Delete(AttributeEntity entity);
        Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default);
    }
    public class AttributeRepository : IAttributeRepository
    {
        protected readonly LookUpDbContext _context;

        public AttributeRepository(LookUpDbContext context) { 
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<AttributeEntity> Add(AttributeEntity entity)
        {
            _context.Set<AttributeEntity>().Add(entity);
            return entity;
        }

        public void Delete(AttributeEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<AttributeEntity>> GetAll()
        {
            return await _context.Set<AttributeEntity>()
                .Where(u => u.Status != LookUpStatusEnum.Removed).ToListAsync();
        }

        public async Task<AttributeEntity> GetById(Guid id)
        {
            return await _context.Set<AttributeEntity>().FindAsync(id);
        }

        public Task<Attribute> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<AttributeEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<AttributeEntity> Update(AttributeEntity entity)
        {
            _context.Set<AttributeEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if(disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
