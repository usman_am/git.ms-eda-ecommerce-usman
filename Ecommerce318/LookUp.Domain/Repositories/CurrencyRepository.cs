﻿

using LookUp.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace LookUp.Domain.Repositories
{
    public interface ICurrencyRepository
    {
        Task<IEnumerable<CurrecyEntity>> GetAll();
        Task<CurrecyEntity> GetById(Guid id);
        Task<CurrecyEntity> Add(CurrecyEntity entity);
        Task<CurrecyEntity> Update(CurrecyEntity entity);
        void Delete(CurrecyEntity entity);
        Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default);
    }

    public class CurrencyRepository : ICurrencyRepository
    { 
        protected readonly LookUpDbContext _context;

        public CurrencyRepository(LookUpDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<CurrecyEntity> Add(CurrecyEntity entity)
        {
            _context.Set<CurrecyEntity>().Add(entity);
            return entity;
        }

        public async Task<IEnumerable<CurrecyEntity>> GetAll()
        {
            return await _context.Set<CurrecyEntity>()
                .Where(u => u.Status != LookUpStatusEnum.Removed).ToListAsync();
        }

        public async Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public void Delete(CurrecyEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<CurrecyEntity> GetById(Guid id)
        {
            return await _context.Set<CurrecyEntity>().FindAsync(id);
        }

        public async Task<CurrecyEntity> Update(CurrecyEntity entity)
        {
            _context.Set<CurrecyEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

    }

   
}
