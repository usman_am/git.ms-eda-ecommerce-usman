﻿using LookUp.Domain.EventEnvelopes.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.EventEnvelopes.Currency
{
    public record CurrencyDeleted(
        Guid Id,
        LookUpStatusEnum Status)
    {
        public static CurrencyDeleted Delete(
            Guid id,
            LookUpStatusEnum status
            ) => new(id, status);
}
}
