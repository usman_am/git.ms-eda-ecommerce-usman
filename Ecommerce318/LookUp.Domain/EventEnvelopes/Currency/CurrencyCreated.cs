﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.EventEnvelopes.Currency
{
    public record CurrencyCreated(
        Guid Id,
        string Name,
        string Code,
        string Symbol,
        LookUpStatusEnum Status,
        DateTime Modified
        )
    {
        public static CurrencyCreated Create(
            Guid id,
            string name,
            string code,
            string symbol,
            LookUpStatusEnum status,
            DateTime modified
            ) => new(id, name, code, symbol, status, modified);
    }
}
