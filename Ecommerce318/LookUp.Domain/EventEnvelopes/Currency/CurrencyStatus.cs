﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.EventEnvelopes.Currency
{
    public record CurrencyStatus(
        Guid Id,
        LookUpStatusEnum Status,
        DateTime Modified
        )
    {
        public static CurrencyStatus EditStatus(
            Guid id,
            LookUpStatusEnum status,
            DateTime modified
            ) => new(id, status, modified);
    }
}
