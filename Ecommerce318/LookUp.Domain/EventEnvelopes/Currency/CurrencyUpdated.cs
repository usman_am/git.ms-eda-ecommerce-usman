﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.EventEnvelopes.Currency
{
    public record CurrencyUpdated(
        Guid Id,
        string Name,
        string Code,
        string Symbol,
        DateTime Modified
        )
    {
        public static CurrencyUpdated Update(
            Guid id,
            string name,
            string code,
            string symbol,
            DateTime modified
            ) => new(id, name, code, symbol, modified);
    }
}
