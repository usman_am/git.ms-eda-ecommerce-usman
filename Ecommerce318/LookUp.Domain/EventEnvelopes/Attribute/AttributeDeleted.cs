﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.EventEnvelopes.Attribute
{
    public record AttributeDeleted(
        Guid Id,
        LookUpStatusEnum Status)
    {
        public static AttributeDeleted Delete(
            Guid id,
            LookUpStatusEnum status
            ) => new(id,status);
    }
}
