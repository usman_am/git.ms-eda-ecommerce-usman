﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.EventEnvelopes.Attribute
{
    public record AttributeStatus(
        Guid Id,
        LookUpStatusEnum Status,
        DateTime Modified)
    {
        public static AttributeStatus EditStatus(
           Guid id,
           LookUpStatusEnum status,
            DateTime modified
            ) => new(id, status, modified);
    }
}
