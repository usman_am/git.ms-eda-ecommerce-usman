﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using LookUp.Domain.Dtos;
using LookUp.Domain.Entities;
using LookUp.Domain.EventEnvelopes.Attribute;
using LookUp.Domain.EventEnvelopes.Currency;
using LookUp.Domain.Repositories;

namespace LookUp.Domain.Services
{
    public interface ICurrencyServices
    {
        Task<IEnumerable<CurrencyDto>> All();
        Task<CurrencyDto> AddCurrency(CurrencyDto dto);
        Task<CurrencyDto> GetCurrencyById(Guid id);
        Task<bool> UpdateCurrency(CurrencyDto dto);
        Task<bool> UpdateCurrencyStatus(Guid id, LookUpStatusEnum status);
        Task<bool> DeleteCurrency(Guid id);

    }
    public class CurrencyService: ICurrencyServices
    {
        private ICurrencyRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public CurrencyService(ICurrencyRepository repository, IMapper mapper,
            IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<CurrencyDto> AddCurrency(CurrencyDto dto)
        {
            if (dto != null)
            {
                dto.Status = LookUpStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<CurrecyEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsyc();

                if (result > 0)
                {
                    var externalEvent = new EventEnvelope<CurrencyCreated>(
                    CurrencyCreated.Create(
                        entity.Id,
                        entity.Name,
                        entity.Code,
                        entity.Symbol,
                        entity.Status,
                        entity.Modified
                    )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return _mapper.Map<CurrencyDto>(entity);
                }
                
            }
            return new CurrencyDto();
        }

        public async Task<IEnumerable<CurrencyDto>> All()
        {
            return _mapper.Map<IEnumerable<CurrencyDto>>(await _repository.GetAll());
        }

        public async Task<CurrencyDto> GetCurrencyById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null && result.Status != LookUpStatusEnum.Removed)
                {
                    return _mapper.Map<CurrencyDto>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateCurrency(CurrencyDto dto)
        {
            if (dto != null)
            {
                var currency = await _repository.GetById(dto.Id);
                if (currency != null && currency.Status != LookUpStatusEnum.Removed)
                {
                    dto.Status = currency.Status;
                    var entity = await _repository.Update(_mapper.Map<CurrecyEntity>(dto));
                    var result = await _repository.SaveChangesAsyc();
                    var externalEvent = new EventEnvelope<CurrencyUpdated>(
                    CurrencyUpdated.Update(
                        entity.Id,
                        entity.Name,
                        entity.Code,
                        entity.Symbol,
                        entity.Modified
                    )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                    if (result > 0) return true;
                }
            }
            return false;
        }

        public async Task<bool> UpdateCurrencyStatus(Guid id, LookUpStatusEnum status)
        {

            var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.Status = status;
                var entity = await _repository.Update(attribute);
                var result = await _repository.SaveChangesAsyc();
                var externalEvent = new EventEnvelope<CurrencyStatus>(
                    CurrencyStatus.EditStatus(
                        entity.Id,
                        entity.Status,
                        entity.Modified
                    )
                    );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                if (result > 0) return true;
            }

            return false;
        }

        public async Task<bool> DeleteCurrency(Guid id)
        {

            var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.Status = LookUpStatusEnum.Removed;
                var entity = await _repository.Update(attribute);
                var result = await _repository.SaveChangesAsyc();
                var externalEvent = new EventEnvelope<CurrencyDeleted>(
                    CurrencyDeleted.Delete(
                        entity.Id,
                        entity.Status
                    )
                    );
                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                if (result > 0) return true;
            }
            return false;
        }
    }

    
}
