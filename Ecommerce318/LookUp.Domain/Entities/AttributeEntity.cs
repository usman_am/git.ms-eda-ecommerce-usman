﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Entities
{
    public class AttributeEntity
    {//prob
        public Guid Id { get; set; }
        public AttributeTypeEnum Type { get; set; } = default!;
        public string Unit { get; set; } = default!;
        public LookUpStatusEnum Status { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;
    }
}
