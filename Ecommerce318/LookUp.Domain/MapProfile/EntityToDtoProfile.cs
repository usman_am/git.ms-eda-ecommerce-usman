﻿using AutoMapper;
using LookUp.Domain.Dtos;
using LookUp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.MapProfile
{
    public class EntityToDtoProfile: Profile
    {
        public EntityToDtoProfile(): base("Entity to Dto profile")
        {
            CreateMap<AttributeEntity, AttributeDto>();//mapping
            CreateMap<AttributeDto, AttributeEntity>();
        }
    }
}
