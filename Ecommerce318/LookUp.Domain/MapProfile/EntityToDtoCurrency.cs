﻿using AutoMapper;
using LookUp.Domain.Dtos;
using LookUp.Domain.Entities;

namespace LookUp.Domain.MapProfile
{
    public class EntityToDtoCurrency: Profile
    {
        public EntityToDtoCurrency(): base("Entity to Dto Currencies")
        {
            CreateMap<CurrecyEntity, CurrencyDto>();
            CreateMap<CurrencyDto, CurrecyEntity>();
        }
    }
}
