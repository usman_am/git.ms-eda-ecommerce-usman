﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Store.Domain.Entities;

namespace Store.Domain
{
    public static class ServiceExtention
    {
        public static void AddDomainContext(this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionAction,
            ServiceLifetime contextLifetime = ServiceLifetime.Scoped,//diberi batas waktu koneksi lifetime 10 detik
            ServiceLifetime optionLifetime = ServiceLifetime.Scoped)
        {
            services.AddDbContext<StoreDbContext>(optionAction, contextLifetime,
                optionLifetime);
        }
    }
}
