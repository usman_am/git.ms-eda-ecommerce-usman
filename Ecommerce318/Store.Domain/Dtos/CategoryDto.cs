﻿

namespace Store.Domain.Dtos
{
    public class CategoryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public StoreStatusEnum Status { get; internal set; }
    }
}
