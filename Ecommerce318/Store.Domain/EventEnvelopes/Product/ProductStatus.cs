﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.EventEnvelopes.Product
{
    public record ProductStatus(
        Guid Id,
        StoreStatusEnum Status,
        DateTime Modified
        )
    {
        public static ProductStatus EditStatus(
            Guid id,
            StoreStatusEnum status,
            DateTime modified
            ) => new(id,status, modified);
    }
}
