﻿using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Repositories
{
    public interface IProductRepository
    {
        Task<Attribute> GetCount();
        Task<IEnumerable<ProductEntity>> GetAll();
        Task<IEnumerable<ProductEntity>> GetPaged(int page, int size);
        Task<ProductEntity> GetById(Guid id);
        Task<ProductEntity> Add(ProductEntity entity);
        Task<ProductEntity> Update(ProductEntity entity);
        void Delete(ProductEntity entity);
        Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default);
    }
    public class ProductRepository: IProductRepository
    {
        protected readonly StoreDbContext _context;

        public ProductRepository(StoreDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<ProductEntity> Add(ProductEntity entity)
        {
            _context.Set<ProductEntity>().Add(entity);
            return entity;
        }

        public void Delete(ProductEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ProductEntity>> GetAll()
        {
            return await _context.Set<ProductEntity>().ToListAsync();
        }

        public async Task<ProductEntity> GetById(Guid id)
        {
            return await _context.Set<ProductEntity>().FindAsync(id);
        }

        public Task<Attribute> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ProductEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsyc(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<ProductEntity> Update(ProductEntity entity)
        {
            _context.Set<ProductEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    
}
