﻿using AutoMapper;
using Framework.Core.Events.Externals;
using Framework.Core.Events;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.Repositories;

namespace Store.Domain.Services
{
    public interface IAttributeService
    {
        Task<IEnumerable<AttributeDto>> All();
        Task<AttributeDto> GetAttributeById(Guid id);
        Task<AttributeDto> AddAttribute(AttributeDto dto);
        Task<bool> UpdateAttribute(AttributeDto dto);
        Task<bool> UpdateAttributeStatus(Guid id, LookUpStatusEnum status);
    }
    public class AttributeService : IAttributeService
    {
        private IAttributeRepository _repository;
        private readonly IMapper _mapper;
        public AttributeService(IAttributeRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<AttributeDto> AddAttribute(AttributeDto dto)
        {
            if (dto != null)
            {
                dto.Status = LookUpStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<AttributeEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsyc();

                if (result > 0)
                    return _mapper.Map<AttributeDto>(entity);
            }
            return new AttributeDto();
        }

        public async Task<IEnumerable<AttributeDto>> All()
        {
            return _mapper.Map<IEnumerable<AttributeDto>>(await _repository.GetAll());
        }

        public async Task<AttributeDto> GetAttributeById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<AttributeDto>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateAttribute(AttributeDto dto)
        {
            if (dto != null)
            {
                var attribute = await _repository.GetById(dto.Id);
                if (attribute != null)
                {
                    var entity = await _repository.Update(_mapper.Map<AttributeEntity>(dto));
                    var result = await _repository.SaveChangesAsyc();

                    if (result > 0) return true;
                }
            }
            return false;
        }

        public async Task<bool> UpdateAttributeStatus(Guid id, LookUpStatusEnum status)
        {
            var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.Status = status;
                var entity = await _repository.Update(attribute);
                var result = await _repository.SaveChangesAsyc();
                if (result > 0) return true;
            }
            return false;
        }

        public async Task<bool> UpdateAttributeStandard(AttributeDto dto)
        {
            if (dto != null)
            {
                var attribute = await _repository.GetById(dto.Id);
                if (attribute != null)
                {
                    dto.Status = attribute.Status;
                    var entity = await _repository.Update(_mapper.Map<AttributeEntity>(dto));
                    var result = await _repository.SaveChangesAsyc();
                    if (result > 0) return true;
                }
            }
            return false;
        }
    }
}
