﻿using AutoMapper;
using Framework.Core.Events.Externals;
using Framework.Core.Events;
using Store.Domain.Dtos;
using Store.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Store.Domain.Entities;

namespace Store.Domain.Services
{
    public interface IGalleryService
    {
        Task<IEnumerable<GalleryDto>> All();
        Task<GalleryDto> GetGalleryById(Guid id);
        Task<GalleryDto> AddGallery(GalleryDto dto);
        Task<bool> UpdateGallery(GalleryDto dto);
        Task<bool> UpdateGalleryStatus(Guid id, StoreStatusEnum status);
        Task<bool> DeleteGallery(Guid id);
    }
    public class GalleryService : IGalleryService
    {
        private IGalleryRepository _repository;
        private readonly IMapper _mapper;

        public GalleryService(IGalleryRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<GalleryDto> AddGallery(GalleryDto dto)
        {
            if (dto != null)
            {
                dto.Status = StoreStatusEnum.Active;
                var dtoToEntity = _mapper.Map<GalleryEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    return _mapper.Map<GalleryDto>(entity);
                }
            }
            return new GalleryDto();
        }

        public async Task<IEnumerable<GalleryDto>> All()
        {
            return _mapper.Map<IEnumerable<GalleryDto>>(await _repository.getAll());    
        }

        public Task<bool> DeleteGallery(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<GalleryDto> GetGalleryById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<GalleryDto>(result);
                }
            }
            return null;
        }

        public Task<bool> UpdateGallery(GalleryDto dto)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateGalleryStatus(Guid id, StoreStatusEnum status)
        {
            throw new NotImplementedException();
        }
    }


}
