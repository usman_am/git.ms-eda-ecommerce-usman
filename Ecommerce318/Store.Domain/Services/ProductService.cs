﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.EventEnvelopes.Product;
using Store.Domain.Repositories;

namespace Store.Domain.Services
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDto>> All();
        Task<ProductDto> GetProductById(Guid id);
        Task<ProductDto> AddProduct(ProductDto dto);
        Task<bool> UpdateProduct(ProductDto dto);
        Task<bool> UpdateProductStatus(Guid id, StoreStatusEnum status);
        Task<bool> DeleteProduct(Guid id);
    }
    public class ProductService: IProductService
    {
        private IProductRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;
        public ProductService(IProductRepository repository, IMapper mapper
            ,IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<ProductDto> AddProduct(ProductDto dto)
        {
            if (dto != null)
            {
                dto.Status = StoreStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<ProductEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsyc();

                if (result > 0)
                {
                    var externalEvent = new EventEnvelope<ProductCreated>(
                    ProductCreated.Create(
                       entity.Id,
                       entity.CategoryId,
                       entity.AttributeId,
                       entity.SKU,
                       entity.Name,
                       entity.Description,
                       entity.Price,
                       entity.Volume,
                       entity.Sold,
                       entity.Stock,
                       entity.Status,
                       entity.Modified
                   )
                   );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return _mapper.Map<ProductDto>(entity);
                }
            }
            return new ProductDto();
        }

        public async Task<IEnumerable<ProductDto>> All()
        {
            return _mapper.Map<IEnumerable<ProductDto>>(await _repository.GetAll());
        }

        public async Task<ProductDto> GetProductById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<ProductDto>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateProduct(ProductDto dto)
        {
            if (dto != null)
            {
                var attribute = await _repository.GetById(dto.Id);
                if (attribute != null)
                {
                    var entity = await _repository.Update(_mapper.Map<ProductEntity>(dto));
                    var result = await _repository.SaveChangesAsyc();

                    if (result > 0) {
                        var externalEvent = new EventEnvelope<ProductUpdated>(
                        ProductUpdated.Update(
                        entity.Id,
                        entity.CategoryId,
                        entity.AttributeId,
                        entity.SKU,
                        entity.Name,
                        entity.Description,
                        entity.Price,
                        entity.Volume,
                        entity.Sold,
                        entity.Stock,
                        entity.Modified
                        )
                        );
                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                    
                }
            }
            return false;
        }

        public async Task<bool> UpdateProductStandard(ProductDto dto)
        {
            if (dto != null)
            {
                var attribute = await _repository.GetById(dto.Id);
                if (attribute != null)
                {
                    dto.Status = attribute.Status;
                    var entity = await _repository.Update(_mapper.Map<ProductEntity>(dto));
                    var result = await _repository.SaveChangesAsyc();

                    if (result > 0) return true;
                }
            }
            return false;
        }

        public async Task<bool> DeleteProduct(Guid id)
        {
            if (id != null)
            {
                var attribute = await _repository.GetById(id);
                if (attribute != null)
                {
                    attribute.Status = StoreStatusEnum.Removed;
                    var entity = await _repository.Update(attribute);
                    var result = await _repository.SaveChangesAsyc();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<ProductDeleted>(
                        ProductDeleted.Delete(
                        entity.Id,
                        entity.Status,
                        entity.Modified
                        )
                        );
                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateProductStatus(Guid id, StoreStatusEnum status)
        {
            var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.Status = status;
                var entity = await _repository.Update(attribute);
                var result = await _repository.SaveChangesAsyc();
                if (result > 0) {
                    var externalEvent = new EventEnvelope<ProductStatus>(
                    ProductStatus.EditStatus(
                    entity.Id,
                    entity.Status,
                    entity.Modified
                    )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return true;
                } 
            }
            return false;
        }
    }


}
