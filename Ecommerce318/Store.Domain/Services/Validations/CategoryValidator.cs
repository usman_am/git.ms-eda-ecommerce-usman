﻿using FluentValidation;
using Store.Domain.Dtos;
namespace Store.Domain.Services.Validations
{
    public class CategoryValidator: AbstractValidator<CategoryDto>
    {
        public CategoryValidator()
        {
            RuleFor(x => x.Name).NotNull().WithMessage("Name cannot null");
            RuleFor(x => x.Description).MinimumLength(5).WithMessage("Descriotion minimum 5 chars");
        }
    }
}
