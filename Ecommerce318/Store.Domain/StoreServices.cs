﻿using Microsoft.Extensions.DependencyInjection;
using Framework.Core.Projection;
using Store.Domain.Projections;

namespace Store.Domain
{
    public static class StoreServices
    {
        public static IServiceCollection AddStore(this IServiceCollection services) =>
            services.AddProjections();

        private static IServiceCollection AddProjections(this IServiceCollection services) =>
            services //.AddScoped<AttributeProjection>(); untuk servis dari lookup ke store
            .Projection(builder => builder
                .AddOn<AttributeCreated>(AttributeProjection.Handle)
                .AddOn<AttributeUpdated>(AttributeProjection.Handle)
                .AddOn<AttributeStatusChanged>(AttributeProjection.Handle)
                .AddOn<AttributeDeleted>(AttributeProjection.Handle)
            );
    }
}
