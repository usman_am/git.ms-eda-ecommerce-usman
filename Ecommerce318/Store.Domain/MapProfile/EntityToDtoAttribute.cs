﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.MapProfile
{
    public class EntityToDtoAttribute: Profile
    {
        public EntityToDtoAttribute() : base("Attribute to Dto profile")
        {
            CreateMap<AttributeEntity, AttributeDto>();//mapping
            CreateMap<AttributeDto, AttributeEntity>();
        }
    }
}
