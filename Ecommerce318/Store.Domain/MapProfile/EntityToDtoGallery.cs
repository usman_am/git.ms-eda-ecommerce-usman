﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.MapProfile
{
    public class EntityToDtoGallery : Profile
    {
        public EntityToDtoGallery() : base("Entity to Dto Gallery")
        {
            CreateMap<GalleryEntity, GalleryDto>();//mapping
            CreateMap<GalleryDto, GalleryEntity>();
        }
    }
}
