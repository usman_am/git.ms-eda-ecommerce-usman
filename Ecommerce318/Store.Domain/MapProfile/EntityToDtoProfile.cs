﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Category to Dto Category")
        {
            CreateMap<CategoryEntity, CategoryDto>();//mapping
            CreateMap<CategoryDto, CategoryEntity>();
        }
    }
}
