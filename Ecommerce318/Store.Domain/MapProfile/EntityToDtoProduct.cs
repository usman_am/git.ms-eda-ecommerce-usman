﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.MapProfile
{
    public class EntityToDtoProduct: Profile
    {
        public EntityToDtoProduct() : base("Category to Dto product")
        {
            CreateMap<ProductEntity, ProductDto>();//mapping
            CreateMap<ProductDto, ProductEntity>();
        }
    }
}
