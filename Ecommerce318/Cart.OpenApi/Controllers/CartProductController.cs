﻿using Cart.Domain.Dtos;
using Cart.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Cart.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartProductController : ControllerBase
    {
        private readonly ICartProductService _services;
        private readonly ILogger<CartProductController> _logger;

        public CartProductController(ICartProductService service, 
            ILogger<CartProductController> logger)
        {
            _services = service;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _services.All());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CartProductDto payload,
            CancellationToken cancellationToken)
        {
            try
            {
                var dto = await _services.AddCartProduct(payload);
                if (dto != null)
                    return Ok(dto);
            }
            catch (OperationCanceledException ex) when
            (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Put(Guid id, [FromBody] CartProductDto payload,
            CancellationToken cancellationToken)
        {
            try
            {
                if (payload != null)
                {
                    payload.Id = id;
                    var isUpdated = await _services.UpdateCartProduct(payload);
                    if (isUpdated)
                        return Ok(isUpdated);
                }
            }
            catch (OperationCanceledException ex) when
                (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }
    }
}
